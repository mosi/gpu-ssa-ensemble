from pathlib import Path
import math
import argparse
import os
import random
import itertools

import jinja2
import numpy as np

from reproduce_code_generation.python_src.react_lang_parser import parse_custom_reaction_lang


def get_argument_parser():
    parser = argparse.ArgumentParser(description="Generate a model specific simulator from a template and a model file",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--replication-count", type=int, default=(2 ** 15), help="Number of replications to simulate")
    parser.add_argument("--max-time", type=float, default=10.0, help="Maximum simulation time")
    parser.add_argument("--template-file", type=str, default="coarse_grained_parallelization.cu",
                        help="Filename of simulator template in ./templates/ directory")
    parser.add_argument("--model-file", type=str, default="./models/decay_dimerization.reaction_model",
                        help="Path to .reaction_model model file")
    parser.add_argument("--single-precision", action="store_true", help="Use single instead of double precision")
    parser.add_argument("--prepare-sort-reactions", action="store_true",
                        help="Generate a simulator that counts the firing reactions and writes the counts to a file")
    parser.add_argument("--sort-reactions", action="store_true", help="Use a reaction counts file to sort the reactions")
    parser.add_argument("--scramble-reactions", action="store_true", help="Scramble the reactions")
    parser.add_argument("--scramble-reactions-seed", type=int, default=42, help="Seed to use for the reaction scrambling")
    parser.add_argument("--verbose", action="store_true", help="Print debugging information")
    parser.add_argument("--time", action="store_true", help="Measure and print execution times of program parts")
    parser.add_argument("--write-results", action="store_true", help="Write simulation trajectories to a csv file")
    parser.add_argument("--disable-runtime-sorting", action="store_true", help="Disables runtime sorting if the template supports it")
    return parser


def create_model(reactants, reactions, model_name, scramble_reactions=False, scramble_reactions_seed=42,
                 sort_reactions=False, replication_count=(2**15), max_time=10.0, prepare_sort_reactions=False, **kwargs):
    for reaction in reactions:
        for value in reaction["actions"].values():
            assert value != 0

    if scramble_reactions:
        random.seed(scramble_reactions_seed)
        random.shuffle(reactions)
    elif sort_reactions:
        with open(Path(__file__).resolve().parent / "reaction_counts" / f"reaction_counts-{model_name}-{int(max_time)}.csv", "r") as f:
            counts = [int(c) for c in f.read().split(",")]
        assert(len(counts) == len(reactions))
        reactions = [reaction for _, reaction in sorted(enumerate(reactions), key=lambda x: counts[x[0]], reverse=True)]

    if not prepare_sort_reactions:
        reactions = [reaction for reaction in reactions if reaction["rate"] != 0.0]

    model = {}
    tmp_keys = list(reactants.keys())
    model["reactant_names"] = [str(key) for key in tmp_keys]
    model["initial_population_counts"] = [reactants[key] for key in tmp_keys]
    model["reaction_rates"] = [reaction["rate"] for reaction in reactions]
    model["reactions"] = [{tmp_keys.index(key): action for key, action in reaction["actions"].items()} for reaction in reactions]
    model["raw_reactions"] = reactions
    model["replication_count"] = replication_count
    model["reaction_count"] = len(reactions)
    model["population_count"] = len(reactants)
    model["max_elapsed_time"] = max_time
    model["dependant_populations"] = []
    model["dependant_populations_minus"] = []  # for propensity functions (A + A -> B: (A - 0) * (A - 1) * rate)
                                               #                                           ---       ---
    for reaction in reactions:
        model["dependant_populations"].append([tmp_keys.index(c) for c in reaction["propensity_dependencies"]])
        model["dependant_populations"][-1].sort()
        minus = []
        i = 0
        for index, d in enumerate(model["dependant_populations"][-1]):
            if index > 0 and model["dependant_populations"][-1][index - 1] != d:
                i = 0
            minus.append(i)
            i += 1
        model["dependant_populations_minus"].append(minus)
    model["dependency_graph"] = []
    for reaction_index, reaction in enumerate(reactions):
        dependencies = set([reaction_index])
        affects = set(tmp_keys.index(key) for key, value in reaction["actions"].items() if value != 0)
        for other_reaction_index in range(len(reactions)):
            affected_by = set(model["dependant_populations"][other_reaction_index])
            if affects.intersection(affected_by):
                dependencies.add(other_reaction_index)
        model["dependency_graph"].append(list(sorted(dependencies)))
    model["initial_propensities"] = []
    for reaction, dependant_populations in zip(reactions, model["dependant_populations"]):
        propensity = reaction["rate"]
        for dependant_population in dependant_populations:
            propensity *= model["initial_population_counts"][dependant_population]
        model["initial_propensities"].append(propensity)
    model["max_populations_to_update_per_reaction"] = max(len(reaction) for reaction in model["reactions"])
    model["max_dependant_reactions_per_reaction"] = max(len(dependencies) for dependencies in model["dependency_graph"])
    model["max_dependant_populations_per_reaction"] = max(len(dependant) for dependant in model["dependant_populations"])

    print("model degree:", sum(len(x) for x in model["dependency_graph"]) / len(model["dependency_graph"]))
    print("species:", model["population_count"])
    print("reactions:", model["reaction_count"])

    assert sum(model["initial_propensities"]) > 0.0

    return model


def get_multi_region_SIR_model(region_count, **create_model_kwargs):
    reactants = {reactant + "_r" + str(i): count for i in range(region_count)
                 for reactant, count in [("S", 3499 if i == 0 else 3500),
                                         ("I", 1 if i == 0 else 0),
                                         ("R", 0)]}

    reactions = [{"rate": 2.0 / 3500,
                  "actions": {"S_r" + str(i): -1, "I_r" + str(i): 1},
                  "propensity_dependencies": ["S_r" + str(i), "I_r" + str(i)]}
                 for i in range(region_count)]
    reactions += [{"rate": 1.0,
                   "actions": {"I_r" + str(i): -1, "R_r" + str(i): 1},
                   "propensity_dependencies": ["I_r" + str(i)]}
                  for i in range(region_count)]

    reactions += [{"rate": 0.1,
                   "actions": {"I_r" + str(i): -1, "I_r" + str(i + 1): 1},
                   "propensity_dependencies": ["I_r" + str(i)]}
                  for i in range(region_count - 1)]
    reactions += [{"rate": 0.1,
                   "actions": {"I_r" + str(i + 1): -1, "I_r" + str(i): 1},
                   "propensity_dependencies": ["I_r" + str(i + 1)]}
                  for i in range(region_count - 1)]

    if region_count >= 3:
        reactions += [{"rate": 0.1,
                       "actions": {"I_r0": -1, "I_r" + str(region_count - 1): 1},
                       "propensity_dependencies": ["I_r0"]}]
        reactions += [{"rate": 0.1,
                       "actions": {"I_r0": 1, "I_r" + str(region_count - 1): -1},
                       "propensity_dependencies": ["I_r" + str(region_count - 1)]}]

    model_name = "SIR_{}".format(region_count)
    return create_model(reactants, reactions, model_name, **create_model_kwargs), model_name


def get_decay_dimerization_model(**create_model_kwargs):
    reactants = {"S1": 10000, "S2": 0, "S3": 0}

    reactions = [{"rate": 1.0, "actions": {"S1": -1}, "propensity_dependencies": ["S1"]},
                 {"rate": 0.002, "actions": {"S1": -2, "S2": 1}, "propensity_dependencies": ["S1", "S1"]},
                 {"rate": 0.5, "actions": {"S2": -1, "S1": 2}, "propensity_dependencies": ["S2"]},
                 {"rate": 0.04, "actions": {"S2": -1, "S3": 1}, "propensity_dependencies": ["S2"]}]

    return create_model(reactants, reactions, "decay_dimerization", **create_model_kwargs), "decay_dimerization"


def get_fully_connected_network_model(**create_model_kwargs):
    reactants = {f"X{i}": 0 for i in range(6)}
    reactants["X0"] = 1000000

    reactions = [{"rate": 1.0,
                  "actions": {a: -1, b: 1},
                  "propensity_dependencies": [a]}
                 for a, b in itertools.permutations(reactants.keys(), 2)]

    return create_model(reactants, reactions, "fully_connected_network", **create_model_kwargs), "fully_connected_network"


def get_custom_reaction_model(filepath, **create_model_kwargs):
    filepath = Path(filepath).resolve(strict=True)
    tmp_cwd = os.getcwd()
    os.chdir("reproduce_code_generation")
    data = parse_custom_reaction_lang(str(filepath))
    os.chdir(tmp_cwd)

    reactants, reactions = {}, []

    assert(len(data["float_constants"]) == 0)
    assert(len(data["int_constants"]) == 0)

    # undo random.shuffle from parse_custom_reaction_lang()
    data["rules"].sort(key=lambda d: d["uid"])

    for k, v in data["species"].items():
        assert(len(v["initial_involved_int_constants"]) == 0)
        assert(len(v["initial_involved_float_constants"]) == 0)
        reactants[str(k)] = int(v["initial"])

    for rule in data["rules"]:
        assert(len(rule["float_constants_in_rate"]) == 0)
        assert(rule["rate_type"] == "complex_rate")
        actions = {}
        _tmp_lefts = {}
        for left in set(rule["full_left"]):
            actions[str(left)] = -rule["full_left"].count(left)
            _tmp_lefts[str(left)] = -(actions[str(left)])
        for right in set(rule["full_right"]):
            a = actions.get(str(right), 0)
            a += rule["full_right"].count(right)
            actions[str(right)] = a
        propensity_dependencies = [d.split(":")[-1] for d in str(rule["rate_expression"]).split("*")[1:]]
        reactions.append({
            "rate": float(rule["rate_expression"].split("*")[0]),
            "actions": actions,
            "propensity_dependencies": propensity_dependencies
        })
        for k, v in _tmp_lefts.items():
            # check if model is compatible with the simulators
            # (populations could underflow if condition is false)
            assert(v == propensity_dependencies.count(k))

    model_name = os.path.splitext(os.path.basename(str(filepath.name)))[0]
    return create_model(reactants, reactions, model_name, **create_model_kwargs), model_name


def generate_model_file(model, model_name):
    print("Generating .reaction_model file for '{}'...".format(model_name))
    try:
        with open("models/" + model_name + ".reaction_model", "x") as f:
            f.write("\nCONSTANTS\n\nSPECIES\n")
            for name, count in zip(model["reactant_names"], model["initial_population_counts"]):
                f.write("{}({});\n".format(name, count))

            f.write("\n\nRULES\n")
            for reaction_index, reaction in enumerate(model["raw_reactions"]):
                actions = reaction["actions"]
                consuming = []
                producing = []
                for key, value in actions.items():
                    assert(value != 0)
                    if value < 0:
                        consuming += [key] * (-value)
                    else:
                        producing += [key] * value
                f.write(" + ".join(consuming))
                f.write(" -> ")
                f.write(" + ".join(producing))
                f.write(" @ ")
                f.write(np.format_float_positional(model["reaction_rates"][reaction_index]) + " * ")
                f.write(" * ".join("#{}".format(c) for c in reaction["propensity_dependencies"]))
                f.write(";\n")
    except FileExistsError:
        print("models/" + model_name + ".reaction_model already exists. Skipping...")
    else:
        print("Model saved as: models/" + model_name + ".reaction_model")


def main():

    parser = get_argument_parser()
    args = parser.parse_args()
    assert(not (args.scramble_reactions and args.sort_reactions))
    assert(not (args.sort_reactions and args.prepare_sort_reactions))
    assert(not (args.scramble_reactions and args.prepare_sort_reactions))

    model, model_name = get_custom_reaction_model(args.model_file, **vars(args))

    template_loader = jinja2.FileSystemLoader(searchpath="./templates/")
    template_env = jinja2.Environment(loader=template_loader)
    template_env.trim_blocks = True
    template_env.lstrip_blocks = True
    template_env.globals.update(zip=zip, len=len, max=max, min=min, enumerate=enumerate, sum=sum, list=list, sorted=sorted)

    def _debug(text):
        print(text)
        return ''

    template_env.filters["debug"] = _debug

    if args.prepare_sort_reactions:
        args.template_file = "coarse_grained_parallelization_with_index_sorting.cu"

    Path("./generated_code/").mkdir(parents=True, exist_ok=True)
    reaction_counts_filepath = Path(__file__).resolve().parent / "reaction_counts" / f"reaction_counts-{model_name}-{int(model['max_elapsed_time'])}.csv"
    reaction_counts_filepath.parent.mkdir(exist_ok=True, parents=True)
    template_kwargs = {
        "model": model,
        "debug": args.verbose,
        "timing": args.time,
        "file_export": args.write_results,
        "count_reactions": args.prepare_sort_reactions,
        "single_precision": args.single_precision,
        "reaction_counts_file_name": repr(str(reaction_counts_filepath)).strip("'"),
        "disable_runtime_sorting": args.disable_runtime_sorting
    }
    for template_file, target_file in ((args.template_file, "generated_code/main.cu"),
                                       ("config.hpp", "generated_code/config.hpp")):
        template = template_env.get_template(template_file)
        with open(target_file, "w") as f:
            f.write(template.render(**template_kwargs))


if __name__ == "__main__":
    main()
