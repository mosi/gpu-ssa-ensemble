#pragma once

#include <iostream>
#include <chrono>
#include <string>
#include <cstring>
#include <cmath>
#include <tuple>
#include <vector>
#include <random>
#include <sstream>

#include <assert.h>

#include <cuda.h>
#include <curand.h>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/tuple.h>
#include <thrust/fill.h>
#include <thrust/sequence.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/for_each.h>
#include <thrust/scan.h>
#include <thrust/remove.h>
#include <thrust/copy.h>

#include "model_parser.cpp"


// #define FILE_EXPORT


#define CUDA_CALL(x) do { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)
#define CURAND_CALL(x) do { if((x)!=CURAND_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)


template <typename T>
struct linear_index_to_column_index : public thrust::unary_function<T, T> {
    T C; // number of columns

    __host__ __device__
    linear_index_to_column_index(T C) : C(C) {}

    __host__ __device__
    T operator()(T i) {
        return i % C;
    }
};


template <typename T>
struct add_to : public thrust::unary_function<T, T> {
    T value;

    __host__ __device__
    add_to(T value) : value(value) {}

    __host__ __device__
    T operator()(unsigned int i) {
        return value + i;
    }
};


template <typename T>
struct multiply_with : public thrust::unary_function<T, T> {
    T value;

    __host__ __device__
    multiply_with(T value) : value(value) {}

    __host__ __device__
    T operator()(unsigned int i) {
        return value * i;
    }
};


struct can_not_react_anymore {
    propensity_type max_elapsed_time;

    __host__ __device__
    can_not_react_anymore(propensity_type max_elapsed_time) : max_elapsed_time(max_elapsed_time) {}
    
    __host__ __device__
    bool operator()(const thrust::tuple<const propensity_type&, const propensity_type&>& variables) {
        return thrust::get<0>(variables) > max_elapsed_time || thrust::get<1>(variables) <= 0.0;
    }
};


struct simulation_step {
    unsigned int species_count;
    unsigned int reaction_count;
    
    __host__ __device__
    simulation_step(unsigned int species_count, unsigned int reaction_count) : species_count(species_count), reaction_count(reaction_count) {}

    __host__ __device__
    void operator()(const thrust::tuple<propensity_type&, propensity_type&, propensity_type&, const propensity_type&, unsigned int*, propensity_type*,
                                        const int*, const propensity_type*, const unsigned int*>& variables) {
        propensity_type& elapsed_time = thrust::get<0>(variables);
        propensity_type& total_propensity = thrust::get<1>(variables);
        propensity_type& random_1 = thrust::get<2>(variables);
        const propensity_type& random_2 = thrust::get<3>(variables);
        unsigned int* populations = thrust::get<4>(variables);
        propensity_type* propensities = thrust::get<5>(variables);
        
        const int* population_updates = thrust::get<6>(variables);
        const propensity_type* reaction_rates = thrust::get<7>(variables);
        const unsigned int* propensity_function_dependencies = thrust::get<8>(variables);

        propensity_type tmp_total_propensity = total_propensity;

        random_1 = random_1 >= 1.0 ? std::nextafter((propensity_type)1.0, (propensity_type)0.0) : random_1;  // prevent inf elapsed time
        elapsed_time += -log((propensity_type)1.0 - random_1) / tmp_total_propensity;

        propensity_type target_distance = random_2 * tmp_total_propensity;
        propensity_type distance = 0.0;
        unsigned int target_reaction;
        for (target_reaction = 0; distance < target_distance && target_reaction < reaction_count; target_reaction++)
            distance += propensities[target_reaction];
        target_reaction--;

        for (int i = 0; i < species_count; i++)
            populations[i] += population_updates[target_reaction * species_count + i];

        propensity_type propensity;
        for (int i = 0; i < reaction_count; i++) {
            propensity = reaction_rates[i];
            for (int j = 0; j < species_count; j++) {
                for (int k = 0; k < propensity_function_dependencies[i * species_count + j]; k++) {
                    propensity *= populations[j] - k;
                }
            }
            propensities[i] = propensity;
        }

        tmp_total_propensity = 0.0;
        for (int i = 0; i < reaction_count; i++)
            tmp_total_propensity += propensities[i];
        total_propensity = tmp_total_propensity;
    }
};


#ifdef FILE_EXPORT
thrust::host_vector<unsigned int> host_indices_map;
thrust::host_vector<propensity_type> host_elapsed_times;
thrust::host_vector<unsigned int> host_populations;


void write_to_file(std::ofstream& out_file, unsigned int running_simulations_count, unsigned int species_count, unsigned int replication_count,
    thrust::device_vector<unsigned int>& indices_map, thrust::device_vector<propensity_type>& elapsed_times, thrust::device_vector<unsigned int>& populations) {

    thrust::copy_n(
        indices_map.begin(),
        running_simulations_count,
        host_indices_map.begin()
    );

    thrust::copy_n(
        elapsed_times.begin(),
        replication_count,
        host_elapsed_times.begin()
    );

    thrust::copy_n(
        populations.begin(),
        replication_count * species_count,
        host_populations.begin()
    );

    out_file << "\n";

    unsigned int map_index = 0;
    for (unsigned int i = 0; i < replication_count; i++) {
        if (i > 0)
            out_file << ",";
        if (host_indices_map[map_index] == i) {
            out_file << host_elapsed_times[i];
            for (int population = 0; population < species_count; population++)
                out_file << "," << host_populations[i * species_count + population];
            map_index++;
        }
        else {
            for (int population = 0; population < species_count; population++)
                out_file << ",";
        }

    }

}
#endif


int main(int argc, char* argv[]) {
    auto init_t1 = std::chrono::high_resolution_clock::now();
    
    if (argc != 4) {
        std::cout << "Usage: simulator.exe path/to/custom_model_file replication_count max_elapsed_time\n\n";
        return EXIT_FAILURE;
    }

    char* model_file = argv[1];
    unsigned int replication_count;  // = 32768
    propensity_type max_elapsed_time;  // = 10.0

    sscanf(argv[2], "%d", &replication_count);
    sscanf(argv[3], "%lf", &max_elapsed_time);
    
    auto model = parse_model(model_file);  // "../../../models/decay_dimerization.custom_model"
    unsigned int species_count = std::get<0>(model);
    unsigned int reaction_count = std::get<1>(model);
    std::string* species = std::get<2>(model);
    unsigned int* tmp_initial_populations = std::get<3>(model);
    propensity_type* orig_reaction_rates = std::get<4>(model);
    int* orig_population_updates = std::get<5>(model);
    unsigned int* orig_propensity_function_dependencies = std::get<6>(model);


    /*
    std::cout << "species_count: " << species_count << "\n";
    std::cout << "reaction_count: " << reaction_count << "\n\n";
    for (int i = 0; i < species_count; i++) {
        std::cout << species[i] << ": " << tmp_initial_populations[i] << "\n";
    }
    std::cout << "\n";
    for (int i = 0; i < reaction_count; i++) {
        std::cout << tmp_reaction_rates[i] << "\npopulation updates:";
        for (int j = 0; j < species_count; j++) {
            std::cout << " " << tmp_population_updates[i * species_count + j];
        }
        std::cout << "\npropensity_function_dependencies:";
        for (int j = 0; j < species_count; j++) {
            std::cout << " " << tmp_propensity_function_dependencies[i * species_count + j];
        }
        std::cout << "\n\n";
    }
    */

    propensity_type* tmp_reaction_rates = new propensity_type[reaction_count]{ 0.0 };
    int* tmp_population_updates = new int[species_count * reaction_count]{ 0 };
    unsigned int* tmp_propensity_function_dependencies = new unsigned int[species_count * reaction_count]{ 0 };

    std::vector<int> indices(reaction_count);
    for (int i = 0; i < reaction_count; i++)
        indices[i] = i;
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(indices.begin(), indices.end(), g);

    for (int i = 0; i < reaction_count; i++) {
        int new_i = indices[i];
        tmp_reaction_rates[new_i] = orig_reaction_rates[i];
        for (int j = 0; j < species_count; j++) {
            tmp_population_updates[new_i * species_count + j] = orig_population_updates[i * species_count + j];
            tmp_propensity_function_dependencies[new_i * species_count + j] = orig_propensity_function_dependencies[i * species_count + j];
        }
    }


    propensity_type* tmp_initial_propensities = new propensity_type[reaction_count];
    propensity_type initial_total_propensity = 0.0;
    for (int i = 0; i < reaction_count; i++) {
        tmp_initial_propensities[i] = tmp_reaction_rates[i];
        for (int j = 0; j < species_count; j++) {
            for (int k = 0; k < tmp_propensity_function_dependencies[i * species_count + j]; k++) {
                tmp_initial_propensities[i] *= tmp_initial_populations[j] - k;
            }
        }
        initial_total_propensity += tmp_initial_propensities[i];
    }


    unsigned int running_simulations_count = replication_count;


    // ### state variables ###
    thrust::device_vector<propensity_type> random_numbers(replication_count * 2);
    
    thrust::device_vector<propensity_type> elapsed_times(replication_count, 0.0);

    thrust::device_vector<propensity_type> total_propensities(replication_count, initial_total_propensity);

    thrust::device_vector<unsigned int> populations(replication_count * species_count);
    thrust::host_vector<unsigned int> initial_populations(tmp_initial_populations, tmp_initial_populations + species_count);
    thrust::copy_n(
        thrust::make_permutation_iterator(
            initial_populations.begin(),
            thrust::make_transform_iterator(
                thrust::counting_iterator<unsigned int>(0),
                linear_index_to_column_index<unsigned int>(species_count)
            )
        ),
        replication_count * species_count,
        populations.begin()
    );

    thrust::device_vector<unsigned int*> population_arrays(replication_count);
    unsigned int* populations_beginning = thrust::raw_pointer_cast(populations.data());
    thrust::copy_n(
        thrust::make_transform_iterator(
            thrust::make_transform_iterator(
                thrust::counting_iterator<unsigned int>(0),
                multiply_with<unsigned int>(species_count)
            ),
            add_to<unsigned int*>(populations_beginning)
        ),
        replication_count,
        population_arrays.begin()
    );

    thrust::device_vector<propensity_type> propensities(replication_count * reaction_count);
    thrust::host_vector<propensity_type> initial_propensities(tmp_initial_propensities, tmp_initial_propensities + reaction_count);
    thrust::copy_n(
        thrust::make_permutation_iterator(
            initial_propensities.begin(),
            thrust::make_transform_iterator(
                thrust::counting_iterator<unsigned int>(0),
                linear_index_to_column_index<unsigned int>(reaction_count)
            )
        ),
        replication_count * reaction_count,
        propensities.begin()
    );

    thrust::device_vector<propensity_type*> propensity_arrays(replication_count);
    propensity_type* propensities_beginning = thrust::raw_pointer_cast(propensities.data());
    thrust::copy_n(
        thrust::make_transform_iterator(
            thrust::make_transform_iterator(
                thrust::counting_iterator<unsigned int>(0),
                multiply_with<unsigned int>(reaction_count)
            ),
            add_to<propensity_type*>(propensities_beginning)
        ),
        replication_count,
        propensity_arrays.begin()
    );

    thrust::device_vector<unsigned int> indices_map(replication_count);
    thrust::sequence(indices_map.begin(), indices_map.end());


    // ### constant model variables (reactions) ###
    int* population_updates;
    cudaMalloc(&population_updates, sizeof(int) * reaction_count * species_count);
    cudaMemcpy(population_updates,
               tmp_population_updates,
               sizeof(int) * reaction_count * species_count,
               cudaMemcpyHostToDevice);

    propensity_type* reaction_rates;
    cudaMalloc(&reaction_rates, sizeof(propensity_type) * reaction_count);
    cudaMemcpy(reaction_rates,
               tmp_reaction_rates,
               sizeof(propensity_type) * reaction_count,
               cudaMemcpyHostToDevice);

    unsigned int* propensity_function_dependencies;
    cudaMalloc(&propensity_function_dependencies, sizeof(unsigned int) * reaction_count * species_count);
    cudaMemcpy(propensity_function_dependencies,
               tmp_propensity_function_dependencies,
               sizeof(unsigned int) * reaction_count * species_count,
               cudaMemcpyHostToDevice);


    #ifdef FILE_EXPORT
    host_indices_map = thrust::host_vector<unsigned int>(replication_count);
    host_elapsed_times = thrust::host_vector<propensity_type>(replication_count);
    host_populations = thrust::host_vector<unsigned int>(replication_count * species_count);

    std::string out_filename;
    out_filename += "out_";
    out_filename += std::to_string(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count());
    out_filename += ".csv";
    std::ofstream out_file(out_filename, std::ios_base::out | std::ios_base::trunc);

    for (unsigned int replication = 0; replication < replication_count; replication++) {
        if (replication)
            out_file << ",";
        out_file << "time_" << replication;
        for (int i = 0; i < species_count; i++)
            out_file << "," << i << "_" << replication;
    }

    write_to_file(out_file, running_simulations_count, species_count, replication_count, indices_map, elapsed_times, populations);
    #endif


    curandGenerator_t gen;
    CURAND_CALL(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
    CURAND_CALL(curandSetPseudoRandomGeneratorSeed(gen, 1234ULL));

    auto init_t2 = std::chrono::high_resolution_clock::now();
    auto init_total_duration = std::chrono::duration_cast<std::chrono::microseconds>(init_t2 - init_t1).count();
    std::cout << ((float)init_total_duration / 1000000.0f) << " seconds (init)\n";

    std::cout << "running simulations: " << running_simulations_count << "\n";
    auto t1 = std::chrono::high_resolution_clock::now();
    unsigned long long total_simulation_steps = 0;

    while (running_simulations_count > 0) {

        CURAND_CALL(curandGenerateUniformDouble(gen, thrust::raw_pointer_cast(random_numbers.data()), running_simulations_count * 2));

        thrust::for_each_n(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(elapsed_times.begin(), indices_map.begin()),
                    thrust::make_permutation_iterator(total_propensities.begin(), indices_map.begin()),
                    random_numbers.begin(),
                    random_numbers.begin() + running_simulations_count,
                    thrust::make_permutation_iterator(population_arrays.begin(), indices_map.begin()),
                    thrust::make_permutation_iterator(propensity_arrays.begin(), indices_map.begin()),
                    thrust::make_constant_iterator(population_updates),
                    thrust::make_constant_iterator(reaction_rates),
                    thrust::make_constant_iterator(propensity_function_dependencies)
                )
            ),
            running_simulations_count,
            simulation_step(species_count, reaction_count)
        );

        #ifdef FILE_EXPORT
        write_to_file(out_file, running_simulations_count, species_count, replication_count, indices_map, elapsed_times, populations);
        #endif
        total_simulation_steps += running_simulations_count;

        auto tmp_iterator = thrust::remove_if(
            indices_map.begin(),
            indices_map.begin() + running_simulations_count,
            thrust::make_permutation_iterator(
                thrust::make_zip_iterator(thrust::make_tuple(elapsed_times.begin(), total_propensities.begin())),
                indices_map.begin()
            ),
            can_not_react_anymore(max_elapsed_time)
        );

        running_simulations_count = tmp_iterator - indices_map.begin();
    }

    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << total_simulation_steps << " total steps\n";
    std::cout << ((float)duration / 1000000.0f) << " seconds (run)\n";
    std::cout << (((double)duration / 1000000.0) / (double)total_simulation_steps * 1000000000.0) << " nanoseconds/step\n";
    std::cout << (((double)total_simulation_steps / ((double)duration / 1000000.0)) / 1000000.0) << " steps/microsecond\n";

    #ifdef FILE_EXPORT
    out_file.close();
    #endif

    return EXIT_SUCCESS;
}
