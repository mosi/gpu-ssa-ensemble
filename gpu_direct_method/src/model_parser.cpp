#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <tuple>


typedef double propensity_type;


std::tuple<unsigned int, unsigned int, std::string*, unsigned int*, propensity_type*, int*, unsigned int*> parse_model(const char* filename) {

    std::map<std::string, unsigned int> species_indices;

    std::string* species;
    unsigned int* initial_populations;
    propensity_type* reaction_rates;
    int* population_updates;
    unsigned int* propensity_function_dependencies;

    unsigned int species_count = 0, reaction_count = 0;
    unsigned int species_index = 0, reaction_index = 0;

	std::ifstream file(filename);

    if (file.fail())
        std::cerr << "Model file not found";

    for (std::string line; std::getline(file, line); ) {
        if (line.empty())
            continue;
        std::istringstream stream(line);

        std::string string_item;
        unsigned int uint_item;

        if (stream >> string_item) {
            
            if (string_item == "species_count") {

                if (!(stream >> species_count)) 
                    std::cerr << "Invalid input (2)\n";

            } else if (string_item == "reaction_count") {

                if (!(stream >> reaction_count))
                    std::cerr << "Invalid input (3)\n";
                
                assert(species_count > 0);

                // TODO: garbage collection
                species = new std::string[species_count];
                initial_populations = new unsigned int[species_count]{ 0 };
                reaction_rates = new propensity_type[reaction_count]{ 0.0 };
                population_updates = new int[species_count * reaction_count]{ 0 };
                propensity_function_dependencies = new unsigned int[species_count * reaction_count]{ 0 };

            } else if (string_item == "species") {

                std::string species_name;
                if (stream >> species_name) {
                    species[species_index] = species_name;
                    species_indices[species_name] = species_index;
                } else
                    std::cerr << "Invalid input (4)\n";

                if (stream >> uint_item) {
                    initial_populations[species_index++] = uint_item;
                } else
                    std::cerr << "Invalid input (5)\n";

            } else if (string_item == "reaction") {

                int mode = 0, species_index;

                if (!(stream >> reaction_rates[reaction_index]))
                    std::cerr << "Invalid input (6)\n";

                while (true) {
                    if (stream >> string_item) {
                        
                        if (string_item == "->") {
                            mode = 1;
                        } else if (string_item == "@") {
                            mode = 2;
                        } else {

                            species_index = species_indices[string_item];

                            if (mode == 0) {
                                population_updates[reaction_index * species_count + species_index]--;
                            } else if (mode == 1) {
                                population_updates[reaction_index * species_count + species_index]++;
                            } else if (mode == 2) {
                                propensity_function_dependencies[reaction_index * species_count + species_index]++;
                            }

                        }

                    } else
                        break;
                }
                reaction_index++;

            } else {
                std::cerr << "Invalid input (1)\n";
            }

        } else {
            std::cerr << "Invalid input (0)\n";
        }
    }

    return std::make_tuple(
        species_count,
        reaction_count,
        species,
        initial_populations,
        reaction_rates,
        population_updates,
        propensity_function_dependencies
    );

}