import subprocess
import os
from pathlib import Path
import time
import json

from run_benchmarks import cmake, msbuild, write_result_data, base_dir


assert(os.name == "nt")  # currently only works on Windows


cmake = "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\Common7\\IDE\\CommonExtensions\\Microsoft\\CMake\\CMake\\bin\\cmake.exe\""
msbuild = "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\MSBuild.exe\""
python = "python"
code_base_dir = base_dir / "gpu_direct_method"
model_base_dir = code_base_dir / "models"
template_name = "gpu_direct_method"
max_run_time = 60 * 15


def convert_models():
    last_error = None
    for i in range(3):  # retries
        try:
            command = python + " \"" + str(base_dir / "convert_models.py") + "\""
            subprocess.check_output(command, shell=True)
        except subprocess.CalledProcessError as e:
            last_error = e
            continue
        else:
            break
    else:
        raise last_error


def build():
    last_error = None
    for i in range(3):  # retries
        try:
            tmp_start = time.time()
            cmake_command = cmake + " -B\"" + str(code_base_dir / "out") + "\" -S\"" + str(code_base_dir) + "\""
            subprocess.check_output(cmake_command, shell=True)

            build_command = msbuild + " /p:Configuration=Release /t:Clean,Build \"" + str(code_base_dir / "out" / "CudaDirectMethod.sln") + "\""
            subprocess.check_output(build_command, shell=True)
            compile_time = time.time() - tmp_start
        except subprocess.CalledProcessError as e:
            last_error = e
            continue
        else:
            break
    else:
        raise last_error
    return compile_time


def run_benchmark(model_file, max_time, replication_exponents, runs, resume=None):
    model_file = model_base_dir / model_file
    if resume is None:
        result_file = base_dir / "results" / "{} - {}.direct_method.json".format(os.path.splitext(model_file.name)[0],
                                                                                 time.time())
        result_file.parent.mkdir(exist_ok=True, parents=True)

        results = {"meta": {"x-label": "Replications",
                            "model_file": str(model_file.name),
                            "max_time": max_time}, "data": {}}

        results["data"][template_name] = {}
    else:
        result_file = resume
        with open(result_file, "r") as f:
            results = json.load(f)

    stop = False
    for replication_exponent in replication_exponents:
        for r in range(runs):
            replication_count = 2 ** replication_exponent
            print(r, "-", template_name, "-", replication_count, "-", model_file.name)

            if resume is not None:
                tmp_key = "$2^{{{0}}}$".format(replication_exponent)
                if r < len(results["data"][template_name].get(tmp_key, {}).get("run_time", [])):
                    print("Skipping...")
                    continue

            run_command = ("\"" + str(code_base_dir / "out" / "Release" / "CudaDirectMethod.exe") + "\" \""
                           + str(model_file) + "\" " + str(int(replication_count)) + " " + str(float(max_time)))
            result_str = subprocess.check_output(run_command, shell=True).decode('utf-8')

            key = "$2^{{{0}}}$".format(replication_exponent)
            write_result_data(results, result_str, template_name, key, generate_code_time=None, compile_time=None)
            if results["data"][template_name][key]["run_time"][-1] > max_run_time:
                stop = True
                if len(results["data"][template_name][key]["run_time"]) == 1:
                    break

            with open(result_file, "w") as f:
                json.dump(results, f, indent=4)
        if stop:
            break


def run_sir_regions_benchmark(replication_count, max_time, regions, runs, resume=None):
    if resume is None:
        result_file = base_dir / "results" / "SIR_regions - {}.direct_method.json".format(time.time())

        results = {"meta": {"x-label": "Regions",
                            "model_file": "SIR_{region_count}.custom_model",
                            "max_time": max_time,
                            "replication_count": replication_count}, "data": {}}

        results["data"][template_name] = {}
    else:
        result_file = resume
        with open(result_file, "r") as f:
            results = json.load(f)

    stop = False
    for region_count in regions:
        for r in range(runs):
            model_file = model_base_dir / f"SIR_{region_count}.custom_model"
            print(r, "-", template_name, "-", replication_count, "-", model_file.name)

            if resume is not None:
                tmp_key = str(region_count)
                if r < len(results["data"][template_name].get(tmp_key, {}).get("run_time", [])):
                    print("Skipping...")
                    continue

            run_command = ("\"" + str(code_base_dir / "out" / "Release" / "CudaDirectMethod.exe") + "\" \""
                           + str(model_file) + "\" " + str(int(replication_count)) + " " + str(float(max_time)))
            result_str = subprocess.check_output(run_command, shell=True).decode('utf-8')

            key = str(region_count)
            write_result_data(results, result_str, template_name, key, generate_code_time=None, compile_time=None)
            if results["data"][template_name][key]["run_time"][-1] > max_run_time:
                stop = True
                if len(results["data"][template_name][key]["run_time"]) == 1:
                    break

            with open(result_file, "w") as f:
                json.dump(results, f, indent=4)
        if stop:
            break


def main():
    runs = 5

    print("Converting models...")
    convert_models()
    print("Building...")
    build_time = build()
    print(f"Build time: {build_time} seconds")

    run_benchmark(model_file="multisite2.custom_model",
                  max_time=4.0,
                  replication_exponents=[10, 11, 12, 13, 14, 15, 16],
                  runs=runs)

    run_benchmark(model_file="decay_dimerization.custom_model",
                  max_time=10.0,
                  replication_exponents=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                                         21, 22, 23, 24, 25, 26],
                  runs=1)

    run_sir_regions_benchmark(replication_count=(2 ** 15),
                              max_time=20.0,
                              regions=[1, 2, 5, 10, 20, 50, 100, 200, 500],
                              runs=runs)

    run_sir_regions_benchmark(replication_count=(2 ** 20),
                              max_time=10.0,
                              regions=[1, 2, 5, 10, 20, 50],
                              runs=runs)

    run_benchmark(model_file="multistate.custom_model",
                  max_time=10.0,
                  replication_exponents=[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                  runs=runs)

    run_benchmark(model_file="decay_dimerization.custom_model",
                  max_time=10.0,
                  replication_exponents=[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                  runs=runs)


if __name__ == "__main__":
    main()
