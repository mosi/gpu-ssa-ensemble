from pathlib import Path
import os

from reproduce_code_generation.python_src.react_lang_parser import parse_custom_reaction_lang

from generate_code import get_custom_reaction_model


base_dir = Path(__file__).parent
source_model_dir = base_dir / "models"
target_model_dir = base_dir / "gpu_direct_method" / "models"
target_model_dir.mkdir(parents=True, exist_ok=True)


def main():
    for file in (path for path in source_model_dir.glob("*.reaction_model") if path.is_file()):
        model, model_name = get_custom_reaction_model(file)
        with open(target_model_dir / f"{model_name}.custom_model", "w") as f:
            f.write(f"species_count {len(model['reactant_names'])}\n")
            f.write(f"reaction_count {len(model['raw_reactions'])}\n")
            for species, initial_population in zip(model["reactant_names"], model["initial_population_counts"]):
                assert(" " not in species)
                assert(int(initial_population) >= 0)
                assert(species not in ["->", "@"])
                f.write(f"species {species} {int(initial_population)}\n")
            for reaction in model["raw_reactions"]:
                f.write(f"reaction {reaction['rate']}")
                left = []
                right = []
                for species, action in reaction["actions"].items():
                    assert(action != 0)
                    if action < 0:
                        left += [species] * (-action)
                    else:
                        right += [species] * action
                for l in left:
                    f.write(f" {l}")
                f.write(" ->")
                for r in right:
                    f.write(f" {r}")
                f.write(f" @")
                for d in reaction["propensity_dependencies"]:
                    f.write(f" {d}")
                f.write("\n")


if __name__ == "__main__":
    main()
