import subprocess
import os
from pathlib import Path
import time
import json


assert(os.name == "nt")  # currently only works on Windows


cmake = "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\Common7\\IDE\\CommonExtensions\\Microsoft\\CMake\\CMake\\bin\\cmake.exe\""
msbuild = "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\MSBuild.exe\""

base_dir = Path(__file__).resolve().parent
max_run_time = 1000


def build():
    last_error = None
    for i in range(3):  # retries
        try:
            tmp_start = time.time()
            cmake_command = cmake + " -B\"" + os.path.join(base_dir, "out") + "\" -S\"" + str(base_dir) + "\""
            subprocess.check_output(cmake_command, shell=True)

            build_command = msbuild + " /p:Configuration=Release \"" + os.path.join(base_dir, "out", "CudaOptimizedDirectMethod.sln") + "\""
            subprocess.check_output(build_command, shell=True)
            compile_time = time.time() - tmp_start
        except subprocess.CalledProcessError as e:
            last_error = e
            continue
        else:
            break
    else:
        raise last_error
    return compile_time


def write_result_data(results, result_str, template_name, key, generate_code_time, compile_time):
    lines = result_str.splitlines()
    assert("seconds (init)" in lines[0])
    assert("total steps" in lines[2])
    assert("seconds (run)" in lines[3])
    res = results["data"][template_name].get(key, {})
    res["steps"] = res.get("steps", [])
    res["steps"].append(int(lines[2].split()[0]))
    res["run_time"] = res.get("run_time", [])
    res["run_time"].append(float(lines[3].split()[0]))
    res["init_time"] = res.get("init_time", [])
    res["init_time"].append(float(lines[0].split()[0]))
    if generate_code_time is not None:
        res["code_gen_time"] = res.get("code_gen_time", [])
        res["code_gen_time"].append(generate_code_time)
    if compile_time is not None:
        res["compile_time"] = res.get("compile_time", [])
        res["compile_time"].append(compile_time)

    if template_name == "Fat Kernel + Sortierung" or template_name == "Fat Kernel + Zustands-Sortierung":
        assert(lines[6].startswith("sorting time: "))
        res["sort_time"] = res.get("sort_time", [])
        res["sort_time"].append(float(lines[6].split(" ")[2]))

    results["data"][template_name][key] = res


def count_reactions(model_file, max_time, replication_count, single_precision=False):
    model_name = os.path.splitext(os.path.basename(str(model_file)))[0]
    reaction_counts_filepath = base_dir / "reaction_counts" / f"reaction_counts-{model_name}-{int(max_time)}.csv"
    if not reaction_counts_filepath.is_file():
        print("Preparing sort for", model_name)
        command = ["python", "generate_code.py",
                   "--replication-count={}".format(replication_count),
                   "--max-time={}".format(max_time),
                   "--prepare-sort-reactions",
                   "--model-file=models/{}".format(model_file)]
        if single_precision:
            command.append("--single-precision")
        subprocess.check_output(command)
        build()
        run_command = "\"" + os.path.join(base_dir, "out", "Release", "CudaOptimizedDirectMethod.exe") + "\""
        subprocess.check_output(run_command, shell=True)
    else:
        print("Skip preparing sort for", model_name)


def run_benchmark(model_file, max_time, replication_exponents, templates, runs, resume=None, single_precision=False):
    if resume is None:
        result_file = Path(__file__).resolve().parent / "results" / "{} - {}{}.json".format(os.path.splitext(model_file)[0],
                                                                                            time.time(),
                                                                                            " - single_precision" if single_precision else "")
        result_file.parent.mkdir(exist_ok=True, parents=True)

        results = {"meta": {"x-label": "Replications",
                            "model_file": model_file,
                            "max_time": max_time}, "data": {}}

        for template_name, _ in templates:
            results["data"][template_name] = {}
    else:
        result_file = resume
        with open(result_file, "r") as f:
            results = json.load(f)

    for template_name, template_file in templates:
        early_stop = False
        for replication_exponent in replication_exponents:
            if early_stop:
                break
            for r in range(runs):
                replication_count = 2 ** replication_exponent
                print(r, "-", template_name, "-", replication_count, "-", model_file)

                if resume is not None:
                    tmp_key = "$2^{{{0}}}$".format(replication_exponent)
                    if r < len(results["data"][template_name].get(tmp_key, {}).get("run_time", [])):
                        print("Skipping...")
                        continue

                tmp_start = time.time()

                ### use new non sorting method
                if template_name == "Fat Kernel":
                    template_file = "coarse_grained_parallelization_with_index_sorting.cu"
                ###

                command = ["python", "generate_code.py",
                           "--replication-count={}".format(replication_count),
                           "--template-file={}".format(template_file),
                           "--max-time={}".format(max_time),
                           "--model-file=models/{}".format(model_file),
                           # "--scramble-reactions",
                           "--sort-reactions"]

                ### use new non sorting method
                if template_name == "Fat Kernel":
                    command.append("--disable-runtime-sorting")
                ###

                if single_precision:
                    command.append("--single-precision")
                subprocess.check_output(command)
                generate_code_time = time.time() - tmp_start

                compile_time = build()

                run_command = "\"" + os.path.join(base_dir, "out", "Release", "CudaOptimizedDirectMethod.exe") + "\""
                try:
                    result_str = subprocess.check_output(run_command, shell=True, timeout=max_run_time).decode('utf-8')
                except subprocess.TimeoutExpired:
                    early_stop = True
                    break

                write_result_data(results, result_str, template_name, "$2^{{{0}}}$".format(replication_exponent), generate_code_time, compile_time)

                with open(result_file, "w") as f:
                    json.dump(results, f, indent=4)


def run_sir_regions_benchmark(replication_count, max_time, regions, templates, runs, count_reactions_replication_count, resume=None, single_precision=False):
    if resume is None:
        result_file = Path(__file__).resolve().parent / "results" / "SIR_regions - {}{}.json".format(time.time(),
                                                                                                     " - single_precision" if single_precision else "")

        results = {"meta": {"x-label": "Regions",
                            "model_file": "SIR_{region_count}.reaction_model",
                            "max_time": max_time,
                            "replication_count": replication_count}, "data": {}}

        for template_name, _ in templates:
            results["data"][template_name] = {}
    else:
        result_file = resume
        with open(result_file, "r") as f:
            results = json.load(f)

    for region_count in regions:
        count_reactions(model_file="SIR_{}.reaction_model".format(region_count),
                        max_time=max_time,
                        replication_count=count_reactions_replication_count,
                        single_precision=single_precision)

    for template_name, template_file in templates:
        early_stop = False
        for region_count in regions:
            if early_stop:
                break
            for r in range(runs):
                print(r, "-", template_name, "-", region_count, "-", replication_count)

                if resume is not None:
                    tmp_key = str(region_count)
                    if r < len(results["data"][template_name].get(tmp_key, {}).get("run_time", [])):
                        print("Skipping...")
                        continue

                ### use new non sorting method
                if template_name == "Fat Kernel":
                    template_file = "coarse_grained_parallelization_with_index_sorting.cu"
                ###

                tmp_start = time.time()
                command = ["python", "generate_code.py",
                           "--replication-count={}".format(replication_count),
                           "--template-file={}".format(template_file),
                           "--max-time={}".format(max_time),
                           "--model-file=models/SIR_{}.reaction_model".format(region_count),
                           # "--scramble-reactions",
                           "--sort-reactions"]

                ### use new non sorting method
                if template_name == "Fat Kernel":
                    command.append("--disable-runtime-sorting")
                ###

                if single_precision:
                    command.append("--single-precision")
                subprocess.check_output(command)
                generate_code_time = time.time() - tmp_start

                compile_time = build()

                run_command = "\"" + os.path.join(base_dir, "out", "Release", "CudaOptimizedDirectMethod.exe") + "\""
                try:
                    result_str = subprocess.check_output(run_command, shell=True, timeout=max_run_time).decode('utf-8')
                except subprocess.TimeoutExpired:
                    early_stop = True
                    break

                write_result_data(results, result_str, template_name, str(region_count), generate_code_time, compile_time)

                with open(result_file, "w") as f:
                    json.dump(results, f, indent=4)


def main():
    runs = 5
    count_reactions_replication_count = 2 ** 15

    fat_kernel_template = ("Fat Kernel", None)
    index_sorting_fat_kernel_template = ("Fat Kernel + Sortierung", "coarse_grained_parallelization_with_index_sorting.cu")
    state_sorting_fat_kernel_template = ("Fat Kernel + Zustands-Sortierung", "coarse_grained_parallelization_with_state_sorting.cu")
    main_loop_in_fat_kernel_template = ("Fat Kernel + Main Loop", "coarse_grained_parallelization_with_main_loop_in_kernel.cu")
    thin_kernels_template = ("Thin Kernels", "fine_grained_parallelization.cu")

    for single_precision in (False, ):  # (False, True):

        count_reactions(model_file="decay_dimerization.reaction_model",
                        max_time=10.0,
                        replication_count=count_reactions_replication_count,
                        single_precision=single_precision)

        dimer_exponents = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]

        run_benchmark(model_file="decay_dimerization.reaction_model",
                      max_time=10.0,
                      replication_exponents=dimer_exponents,
                      templates=[fat_kernel_template],
                      runs=1,
                      single_precision=single_precision)
        run_benchmark(model_file="decay_dimerization.reaction_model",
                      max_time=10.0,
                      replication_exponents=dimer_exponents,
                      templates=[index_sorting_fat_kernel_template],
                      runs=1,
                      single_precision=single_precision)
        run_benchmark(model_file="decay_dimerization.reaction_model",
                      max_time=10.0,
                      replication_exponents=dimer_exponents,
                      templates=[state_sorting_fat_kernel_template],
                      runs=1,
                      single_precision=single_precision)
        run_benchmark(model_file="decay_dimerization.reaction_model",
                      max_time=10.0,
                      replication_exponents=dimer_exponents,
                      templates=[main_loop_in_fat_kernel_template],
                      runs=1,
                      single_precision=single_precision)
        """
        run_benchmark(model_file="decay_dimerization.reaction_model",
                      max_time=10.0,
                      replication_exponents=dimer_exponents,
                      templates=[thin_kernels_template],
                      runs=1,
                      single_precision=single_precision)
        """

        run_benchmark(model_file="decay_dimerization.reaction_model",
                      max_time=10.0,
                      replication_exponents=[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                      templates=[fat_kernel_template,
                                 index_sorting_fat_kernel_template,
                                 state_sorting_fat_kernel_template,
                                 main_loop_in_fat_kernel_template,
                                 # thin_kernels_template
                                 ],
                      runs=runs,
                      single_precision=single_precision)

        run_sir_regions_benchmark(replication_count=(2 ** 15),
                                  max_time=20.0,
                                  regions=[1, 2, 5, 10, 20, 50, 100, 200, 500],
                                  templates=[fat_kernel_template,
                                             index_sorting_fat_kernel_template,
                                             state_sorting_fat_kernel_template,
                                             main_loop_in_fat_kernel_template,
                                             # thin_kernels_template
                                             ],
                                  runs=runs,
                                  single_precision=single_precision,
                                  count_reactions_replication_count=count_reactions_replication_count)

        run_sir_regions_benchmark(replication_count=(2 ** 20),
                                  max_time=10.0,
                                  regions=[1, 2, 5, 10, 20, 50],
                                  templates=[fat_kernel_template,
                                             index_sorting_fat_kernel_template,
                                             state_sorting_fat_kernel_template,
                                             main_loop_in_fat_kernel_template,
                                             # thin_kernels_template
                                             ],
                                  runs=runs,
                                  single_precision=single_precision,
                                  count_reactions_replication_count=count_reactions_replication_count)

        count_reactions(model_file="multistate.reaction_model",
                        max_time=10.0,
                        replication_count=count_reactions_replication_count,
                        single_precision=single_precision)

        run_benchmark(model_file="multistate.reaction_model",
                      max_time=10.0,
                      replication_exponents=[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                      templates=[fat_kernel_template,
                                 index_sorting_fat_kernel_template,
                                 state_sorting_fat_kernel_template,
                                 main_loop_in_fat_kernel_template,
                                 # thin_kernels_template
                                 ],
                      runs=runs,
                      single_precision=single_precision)

        count_reactions(model_file="multisite2.reaction_model",
                        max_time=4.0,
                        replication_count=count_reactions_replication_count,
                        single_precision=single_precision)

        run_benchmark(model_file="multisite2.reaction_model",
                      max_time=4.0,
                      replication_exponents=[10, 11, 12, 13, 14, 15, 16],
                      templates=[fat_kernel_template,
                                 index_sorting_fat_kernel_template,
                                 state_sorting_fat_kernel_template,
                                 main_loop_in_fat_kernel_template,
                                 # thin_kernels_template
                                 ],
                      runs=runs,
                      single_precision=single_precision)


if __name__ == "__main__":
    main()
