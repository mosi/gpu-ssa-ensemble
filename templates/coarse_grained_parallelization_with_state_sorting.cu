#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <string>
#include <cstring>
#include <cmath>

#include <assert.h>

#include <cuda.h>
#include <curand.h>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/tuple.h>
#include <thrust/fill.h>
#include <thrust/sequence.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/for_each.h>
#include <thrust/execution_policy.h>
#include <thrust/binary_search.h>
#include <thrust/scan.h>
#include <thrust/remove.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <thrust/transform.h>
#include <thrust/reduce.h>
#include <thrust/gather.h>

#include "config.hpp"


struct PopulationsWrapper {
    unsigned int populations[POPULATION_COUNT] = { {% for count in model.initial_population_counts %}{{ count }}{% if loop.index < model.population_count %}, {% endif %}{% endfor %} };
};


struct PropensitiesWrapper {
    propensity_type propensities[REACTION_COUNT] = { {% for propensity in model.initial_propensities %}{{ propensity }}{% if loop.index < model.reaction_count %}, {% endif %}{% endfor %} };
};


struct Model {
    thrust::device_vector<propensity_type> elapsed_times;
    thrust::device_vector<propensity_type> total_propensities;
    thrust::device_vector<PopulationsWrapper> populations;
    thrust::device_vector<PropensitiesWrapper> propensities;
    thrust::device_vector<unsigned int> next_reactions;

    thrust::device_vector<unsigned int> indices_map;

    Model() {
        elapsed_times = thrust::device_vector<propensity_type>(REPLICATION_COUNT, 0.0);
        total_propensities = thrust::device_vector<propensity_type>(REPLICATION_COUNT, {{ sum(model.initial_propensities) }});
        populations = thrust::device_vector<PopulationsWrapper>(REPLICATION_COUNT);
        propensities = thrust::device_vector<PropensitiesWrapper>(REPLICATION_COUNT);
        next_reactions = thrust::device_vector<unsigned int>(REPLICATION_COUNT);

        indices_map = thrust::device_vector<unsigned int>(REPLICATION_COUNT);
        thrust::sequence(indices_map.begin(), indices_map.end());
    }
};


struct can_not_react_anymore {
    __host__ __device__
    bool operator()(const thrust::tuple<const propensity_type&, const propensity_type&>& variables) {
        return thrust::get<0>(variables) > MAX_ELAPSED_TIME || thrust::get<1>(variables) <= 0.0;
    }
};


struct update_time_and_choose_next_reaction {
    __host__ __device__
    void operator()(const thrust::tuple<propensity_type&, const propensity_type, propensity_type, const propensity_type, PropensitiesWrapper&, unsigned int&>& variables) {
        /* perform a simulation step (update time and population counts) */
        propensity_type& elapsed_time = thrust::get<0>(variables);
        const propensity_type total_propensity = thrust::get<1>(variables);
        propensity_type random_1 = thrust::get<2>(variables);
        const propensity_type random_2 = thrust::get<3>(variables);
        propensity_type* propensities = thrust::get<4>(variables).propensities;
        unsigned int& target_reaction = thrust::get<5>(variables);

        random_1 = random_1 >= 1.0 ? std::nextafter((propensity_type)1.0, (propensity_type)0.0) : random_1;  // prevent inf elapsed time
        elapsed_time += -LOG((propensity_type)1.0 - random_1) / total_propensity;

        propensity_type target_distance = random_2 * total_propensity;
        propensity_type distance = 0.0;
        unsigned int tmp_target_reaction;
        for (tmp_target_reaction = 0; distance < target_distance && tmp_target_reaction < REACTION_COUNT; tmp_target_reaction++)
            distance += propensities[tmp_target_reaction];
        target_reaction = tmp_target_reaction - 1;
    }
};


struct update_populations_and_propensities_then_update_time_and_choose_next_reaction {
    __host__ __device__
    void operator()(const thrust::tuple<unsigned int&, propensity_type&, PopulationsWrapper&, PropensitiesWrapper&, propensity_type&, propensity_type, const propensity_type>& variables) {
        /* perform a simulation step (update time and population counts) */
        unsigned int& next_reaction = thrust::get<0>(variables);
        propensity_type& total_propensity = thrust::get<1>(variables);
        unsigned int* populations = thrust::get<2>(variables).populations;
        propensity_type* propensities = thrust::get<3>(variables).propensities;
        propensity_type& elapsed_time = thrust::get<4>(variables);
        propensity_type random_1 = thrust::get<5>(variables);
        const propensity_type random_2 = thrust::get<6>(variables);

        switch (next_reaction) {
        {% for reaction_index, reaction in enumerate(model.reactions) %}
        case {{ loop.index0 }}: {
            {% for target_index in sorted(reaction.keys()) %}
            {% if reaction[target_index] < 0 %}
            assert(populations[{{ target_index }}] >= {{ -reaction[target_index] }}u);
            {% endif %}
            {% if reaction[target_index] < 0 %}
            populations[{{ target_index }}] -= {{ -reaction[target_index] }};
            {% else %}
            populations[{{ target_index }}] += {{ reaction[target_index] }};
            {%endif%}
            {% endfor %}

            {% for dependency_index in model.dependency_graph[reaction_index] %}
            propensities[{{ dependency_index }}] = (propensity_type){{ model.reaction_rates[dependency_index] }}{% for dependency, minus in zip(model.dependant_populations[dependency_index], model.dependant_populations_minus[dependency_index]) %} * (populations[{{ dependency }}] - {{ minus }}){% endfor %};
            {% endfor %}

            break;
        }
        {% endfor %}
        default:
            assert(0 && "invalid next_reaction");
            break;
        }

        // TODO: compare performance to thrust reduce_by_key
        propensity_type tmp_total_propensity = 0.0;
        for (int i = 0; i < REACTION_COUNT; i++)
            tmp_total_propensity += propensities[i];
        total_propensity = tmp_total_propensity;


        // start next step:
        random_1 = random_1 >= 1.0 ? std::nextafter((propensity_type)1.0, (propensity_type)0.0) : random_1;  // prevent inf elapsed time
        elapsed_time += -LOG((propensity_type)1.0 - random_1) / tmp_total_propensity;

        propensity_type target_distance = random_2 * tmp_total_propensity;
        propensity_type distance = 0.0;
        unsigned int tmp_target_reaction;
        for (tmp_target_reaction = 0; distance < target_distance && tmp_target_reaction < REACTION_COUNT; tmp_target_reaction++)
            distance += propensities[tmp_target_reaction];
        next_reaction = tmp_target_reaction - 1;
    }
};


#ifdef FILE_EXPORT
thrust::host_vector<unsigned int> host_indices_map(REPLICATION_COUNT);
thrust::host_vector<propensity_type> host_elapsed_times(REPLICATION_COUNT);
thrust::host_vector<PopulationsWrapper> host_populations(REPLICATION_COUNT);


void write_to_file(std::ofstream& out_file, Model& model, unsigned int running_simulations_count, thrust::device_vector<unsigned int>& indices_map) {

    thrust::copy_n(
        indices_map.begin(),
        running_simulations_count,
        host_indices_map.begin()
    );

    thrust::copy_n(
        model.elapsed_times.begin(),
        running_simulations_count,
        host_elapsed_times.begin()
    );

    thrust::copy_n(
        model.populations.begin(),
        running_simulations_count,
        host_populations.begin()
    );

    thrust::sort_by_key(
        host_indices_map.begin(),
        host_indices_map.begin() + running_simulations_count,
        thrust::make_zip_iterator(
            thrust::make_tuple(
                host_elapsed_times.begin(),
                host_populations.begin()
            )
        )
    );

    out_file << "\n";

    unsigned int map_index = 0;
    for (unsigned int i = 0; i < REPLICATION_COUNT; i++) {
        if (i > 0)
            out_file << ",";
        if (host_indices_map[map_index] == i) {
            out_file << host_elapsed_times[map_index];
            for (int population = 0; population < POPULATION_COUNT; population++)
                out_file << "," << host_populations[map_index].populations[population];
            map_index++;
        } else {
            out_file << "{% for i in range(model.population_count) %},{% endfor %}";
        }

    }

}
#endif


int main(int argc, char* argv[]) {

    TIMING_INIT;

    auto init_t1 = std::chrono::high_resolution_clock::now();

    // keeps track of how many simulations are currently running
    unsigned int running_simulations_count = REPLICATION_COUNT;

    unsigned int prev_running_simulations_count = running_simulations_count;

    Model model;

    auto sorting_t0 = std::chrono::high_resolution_clock::now();
    auto sorting_time = std::chrono::duration_cast<std::chrono::microseconds>(sorting_t0 - sorting_t0);

    // vectors for randomly generated variables
    // two random numbers needed for each simulation in each step
    thrust::device_vector<propensity_type> random_numbers_1(REPLICATION_COUNT);
    thrust::device_vector<propensity_type> random_numbers_2(REPLICATION_COUNT);

    thrust::device_vector<propensity_type> tmp_elapsed_times = thrust::device_vector<propensity_type>(REPLICATION_COUNT);
    thrust::device_vector<propensity_type> tmp_total_propensities = thrust::device_vector<propensity_type>(REPLICATION_COUNT);
    thrust::device_vector<PopulationsWrapper> tmp_populations = thrust::device_vector<PopulationsWrapper>(REPLICATION_COUNT);
    thrust::device_vector<PropensitiesWrapper> tmp_propensities = thrust::device_vector<PropensitiesWrapper>(REPLICATION_COUNT);
    thrust::device_vector<unsigned int> tmp_next_reactions = thrust::device_vector<unsigned int>(REPLICATION_COUNT);

    // needed to determine at which index each simulation is; only used for file writing, but always included to make performance measurements more fair
    thrust::device_vector<unsigned int> file_export_model_indices_map = thrust::device_vector<unsigned int>(REPLICATION_COUNT);
    thrust::sequence(file_export_model_indices_map.begin(), file_export_model_indices_map.end());
    thrust::device_vector<unsigned int> tmp_file_export_model_indices_map = thrust::device_vector<unsigned int>(REPLICATION_COUNT);

    #ifdef FILE_EXPORT
    std::string out_filename;
    out_filename += "out_";
    out_filename += std::to_string(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count());
    out_filename += ".csv";
    std::ofstream out_file(out_filename, std::ios_base::out | std::ios_base::trunc);

    for (unsigned int replication = 0; replication < REPLICATION_COUNT; replication++) {
        if (replication)
            out_file << ",";
        out_file << "time_" << replication;
        {% for name in model.reactant_names %}
        out_file << ",{{ name }}_" << replication;
        {% endfor %}
    }

    write_to_file(out_file, model, running_simulations_count, file_export_model_indices_map);
    #endif

    curandGenerator_t gen;
    // create pseudo-random number generator
    CURAND_CALL(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
    // set seed
    CURAND_CALL(curandSetPseudoRandomGeneratorSeed(gen, 1234ULL));

    auto init_t2 = std::chrono::high_resolution_clock::now();
    auto init_total_duration = std::chrono::duration_cast<std::chrono::microseconds>(init_t2 - init_t1).count();
    std::cout << ((float)init_total_duration / 1000000.0f) << " seconds (init)\n";

    // only needed for prints
    std::cout << "running simulations: " << running_simulations_count << "\n";
    auto t1 = std::chrono::high_resolution_clock::now();
    unsigned long long total_simulation_steps = 0;

    TIMING_START;
    CURAND_CALL(CURAND_GENERATE_UNIFORM(gen, thrust::raw_pointer_cast(random_numbers_1.data()), running_simulations_count));
    CURAND_CALL(CURAND_GENERATE_UNIFORM(gen, thrust::raw_pointer_cast(random_numbers_2.data()), running_simulations_count));
    TIMING_STOP("curand");

    TIMING_START;
    thrust::for_each_n(
        // use permutation iterator with the indices map to iterate only through the running simulation
        thrust::make_zip_iterator(
            thrust::make_tuple(
                model.elapsed_times.begin(),
                model.total_propensities.begin(),
                random_numbers_1.begin(),
                random_numbers_2.begin(),
                model.propensities.begin(),
                model.next_reactions.begin()
            )
        ),
        running_simulations_count,
        update_time_and_choose_next_reaction()
    );
    TIMING_STOP("update_time_and_choose_next_reaction");

    // while there are active simulations
    while (running_simulations_count > 0) {

        TIMING_START;
        CURAND_CALL(CURAND_GENERATE_UNIFORM(gen, thrust::raw_pointer_cast(random_numbers_1.data()), running_simulations_count));
        CURAND_CALL(CURAND_GENERATE_UNIFORM(gen, thrust::raw_pointer_cast(random_numbers_2.data()), running_simulations_count));
        TIMING_STOP("curand");

        sorting_t0 = std::chrono::high_resolution_clock::now();
        #ifndef DISABLE_RUNTIME_SORTING
        TIMING_START;
        thrust::gather(
            model.indices_map.begin(),
            model.indices_map.begin() + running_simulations_count,
            model.next_reactions.begin(),
            tmp_next_reactions.begin()
        );
        thrust::sort_by_key(
            tmp_next_reactions.begin(),
            tmp_next_reactions.begin() + running_simulations_count,
            model.indices_map.begin()
        );
        TIMING_STOP("sort by reactions");
        #endif
        TIMING_START;
        thrust::copy_n(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    file_export_model_indices_map.begin(),
                    #ifdef DISABLE_RUNTIME_SORTING
                    model.next_reactions.begin(),
                    #else
                    tmp_next_reactions.begin(),
                    #endif
                    model.total_propensities.begin(),
                    model.populations.begin(),
                    model.propensities.begin(),
                    model.elapsed_times.begin()
                )
            ),
            prev_running_simulations_count,
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    tmp_file_export_model_indices_map.begin(),
                    #ifdef DISABLE_RUNTIME_SORTING
                    tmp_next_reactions.begin(),
                    #else
                    model.next_reactions.begin(),
                    #endif
                    tmp_total_propensities.begin(),
                    tmp_populations.begin(),
                    tmp_propensities.begin(),
                    tmp_elapsed_times.begin()
                )
            )
        );
        thrust::gather(
            model.indices_map.begin(),
            model.indices_map.begin() + running_simulations_count,
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    tmp_file_export_model_indices_map.begin(),
                    #ifdef DISABLE_RUNTIME_SORTING
                    tmp_next_reactions.begin(),
                    #endif
                    tmp_total_propensities.begin(),
                    tmp_populations.begin(),
                    tmp_propensities.begin(),
                    tmp_elapsed_times.begin()
                )
            ),
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    file_export_model_indices_map.begin(),
                    #ifdef DISABLE_RUNTIME_SORTING
                    model.next_reactions.begin(),
                    #endif
                    model.total_propensities.begin(),
                    model.populations.begin(),
                    model.propensities.begin(),
                    model.elapsed_times.begin()
                )
            )
        );
        thrust::sequence(model.indices_map.begin(), model.indices_map.begin() + running_simulations_count);
        TIMING_STOP("gather state variables");
        sorting_time += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - sorting_t0);

        TIMING_START;
        thrust::for_each_n(
            // use permutation iterator with the indices map to iterate only through the running simulation
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    model.next_reactions.begin(),
                    model.total_propensities.begin(),
                    model.populations.begin(),
                    model.propensities.begin(),
                    model.elapsed_times.begin(),
                    random_numbers_1.begin(),
                    random_numbers_2.begin()
                )
            ),
            running_simulations_count,
            update_populations_and_propensities_then_update_time_and_choose_next_reaction()
        );
        TIMING_STOP("update_populations_and_propensities_then_update_time_and_choose_next_reaction");

        #ifdef FILE_EXPORT
        write_to_file(out_file, model, running_simulations_count, file_export_model_indices_map);
        #endif
        total_simulation_steps += running_simulations_count;

        TIMING_START;
        auto tmp_iterator = thrust::remove_if(
            model.indices_map.begin(),
            model.indices_map.begin() + running_simulations_count,
            thrust::make_permutation_iterator(
                thrust::make_zip_iterator(thrust::make_tuple(model.elapsed_times.begin(), model.total_propensities.begin())),
                model.indices_map.begin()
            ),
            can_not_react_anymore()
        );
        TIMING_STOP("can_not_react_anymore");

        prev_running_simulations_count = running_simulations_count;

        // update count of simulations that are still running
        running_simulations_count = tmp_iterator - model.indices_map.begin();
    }

    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << total_simulation_steps << " total steps\n";
    std::cout << ((float)duration / 1000000.0f) << " seconds (run)\n";
    std::cout << (((double)duration / 1000000.0) / (double)total_simulation_steps * 1000000000.0) << " nanoseconds/step\n";
    std::cout << (((double)total_simulation_steps / ((double)duration / 1000000.0)) / 1000000.0) << " steps/microsecond\n";
    std::cout << "sorting time: " << ((double)sorting_time.count() / 1000000.0) << " seconds\n";

    #ifdef FILE_EXPORT
    out_file.close();
    #endif

    return EXIT_SUCCESS;
}
