#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <string>
#include <cstring>
#include <cmath>

#include <assert.h>

#include <cuda.h>
#include <curand.h>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/tuple.h>
#include <thrust/fill.h>
#include <thrust/sequence.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/iterator/transform_output_iterator.h>
#include <thrust/for_each.h>
#include <thrust/binary_search.h>
#include <thrust/scan.h>
#include <thrust/remove.h>
#include <thrust/gather.h>
#include <thrust/find.h>
#include <thrust/copy.h>
#include <thrust/count.h>
#include <thrust/sort.h>

#include "config.hpp"

// TODO?: possible to sort reactions while the simulations are running?

// TODO: compare performance to: sort by next_reactions, then kernel with switch-case for all reactions

// TODO?: populations_propensities_update_index_iterator -> just use tuple_add; remove additional model.population, instead use transform_iterator to return 1 when invalid index

// TODO?: don't use unsigned int

// alternative propensities step 1 idea: pass population iterator (maybe with constant iterator or maybe even with counting iterator) and population count to custom kernel for summing

// TODO: check performance of cuda device api -or- thrust random generators

// TODO: async thrust function calls where possible

// TODO: populations as floats?


// source: https://github.com/NVIDIA/thrust/blob/main/examples/sum_rows.cu
template <typename T>
struct linear_index_to_row_index : public thrust::unary_function<T, T> {
    T C; // number of columns

    __host__ __device__
    linear_index_to_row_index(T C) : C(C) {}

    __host__ __device__
    T operator()(T i) {
        return i / C;
    }
};


template <typename T>
struct linear_index_to_column_index : public thrust::unary_function<T, T> {
    T C; // number of columns

    __host__ __device__
    linear_index_to_column_index(T C) : C(C) {}

    __host__ __device__
    T operator()(T i) {
        return i % C;
    }
};


struct Reactions {
    thrust::device_vector<unsigned int> population_update_indices;
    thrust::device_vector<int> population_updates;

    thrust::device_vector<unsigned int> dependency_indices;
    thrust::device_vector<unsigned int> dependency_indices_with_redundancies;
    thrust::device_vector<propensity_type> dependency_reaction_rates;
    thrust::device_vector<unsigned int> dependant_population_indices;
    thrust::device_vector<int> dependant_populations_minus;

    Reactions() {
        thrust::host_vector<unsigned int> host_population_update_indices(REACTION_COUNT * MAX_POPULATIONS_TO_UPDATE_PER_REACTION, REPLICATION_COUNT * POPULATION_COUNT);
        thrust::host_vector<int> host_population_updates(REACTION_COUNT * MAX_POPULATIONS_TO_UPDATE_PER_REACTION, 0);

        thrust::host_vector<unsigned int> host_dependency_indices(REACTION_COUNT * MAX_DEPENDANT_REACTIONS_PER_REACTION, REPLICATION_COUNT * REACTION_COUNT);
        thrust::host_vector<unsigned int> host_dependency_indices_with_redundancies(REACTION_COUNT * MAX_DEPENDANT_REACTIONS_PER_REACTION);
        thrust::host_vector<propensity_type> host_dependency_reaction_rates(REACTION_COUNT * MAX_DEPENDANT_REACTIONS_PER_REACTION, 0.0);
        thrust::host_vector<unsigned int> host_dependant_population_indices(REACTION_COUNT * MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION, REPLICATION_COUNT * POPULATION_COUNT);
        thrust::host_vector<int> host_dependant_populations_minus(REACTION_COUNT * MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION, 0);

        {% for reaction_index, reaction in enumerate(model.reactions) %}
        // reaction {{ reaction_index }}:
        {% for population_update_index, population_update in reaction.items() %}
        host_population_update_indices[{{ reaction_index * model.max_populations_to_update_per_reaction + loop.index0 }}u] = {{ population_update_index }}u;
        host_population_updates[{{ reaction_index * model.max_populations_to_update_per_reaction + loop.index0 }}u] = {{ population_update }};
        {% endfor %}

        {% for i, dependency_index in enumerate(model.dependency_graph[reaction_index]) %}
        host_dependency_indices[{{ reaction_index * model.max_dependant_reactions_per_reaction + i }}u] = {{ dependency_index }}u;
        {% endfor %}

        {% for j in range(model.max_dependant_reactions_per_reaction) %}
        {% set dependencies = list(model.dependency_graph[reaction_index]) %}
        {% set dependency_index = dependencies[min(j, len(model.dependency_graph[reaction_index]) - 1)] %}
        host_dependency_indices_with_redundancies[{{ reaction_index * model.max_dependant_reactions_per_reaction + j }}u] = {{ dependency_index }}u;
        host_dependency_reaction_rates[{{ reaction_index * model.max_dependant_reactions_per_reaction + j }}u] = {{ model.reaction_rates[dependency_index] }}f;
        {% for dependant_population, dependant_population_minus in zip(model.dependant_populations[dependency_index], model.dependant_populations_minus[dependency_index]) %}
        host_dependant_population_indices[{{ reaction_index * model.max_dependant_reactions_per_reaction * model.max_dependant_populations_per_reaction + j * model.max_dependant_populations_per_reaction + loop.index0 }}u] = {{ dependant_population }}u;
        host_dependant_populations_minus[{{ reaction_index * model.max_dependant_reactions_per_reaction * model.max_dependant_populations_per_reaction + j * model.max_dependant_populations_per_reaction + loop.index0 }}u] = {{ dependant_population_minus }};
        {% endfor %}
        {% endfor %}

        {% endfor %}

        population_update_indices = thrust::device_vector<unsigned int>(host_population_update_indices);
        population_updates = thrust::device_vector<int>(host_population_updates);

        dependency_indices = thrust::device_vector<unsigned int>(host_dependency_indices);
        dependency_indices_with_redundancies = thrust::device_vector<unsigned int>(host_dependency_indices_with_redundancies);
        dependency_reaction_rates = thrust::device_vector<propensity_type>(host_dependency_reaction_rates);
        dependant_population_indices = thrust::device_vector<unsigned int>(host_dependant_population_indices);
        dependant_populations_minus = thrust::device_vector<int>(host_dependant_populations_minus);
    }
};


struct Model {
    thrust::device_vector<propensity_type> elapsed_times;
    thrust::device_vector<unsigned int> populations;

    thrust::device_vector<propensity_type> propensities;

    thrust::device_vector<propensity_type*> propensity_beginnings;

    thrust::device_vector<unsigned int> next_reactions;
    thrust::device_vector<propensity_type> total_propensities;

    thrust::device_vector<unsigned int> indices_map;

    Model() {
        indices_map = thrust::device_vector<unsigned int>(REPLICATION_COUNT);
        thrust::sequence(indices_map.begin(), indices_map.end());

        elapsed_times = thrust::device_vector<propensity_type>(REPLICATION_COUNT, 0.0);
        populations = thrust::device_vector<unsigned int>(REPLICATION_COUNT * POPULATION_COUNT + 1);
        propensities = thrust::device_vector<propensity_type>(REPLICATION_COUNT * REACTION_COUNT);

        thrust::host_vector<propensity_type*> host_propensity_beginnings(REPLICATION_COUNT);
        propensity_beginnings = thrust::device_vector<propensity_type*>(REPLICATION_COUNT);
        propensity_type* propensity_beginning = thrust::raw_pointer_cast(propensities.data());
        for (unsigned int i = 0; i < REPLICATION_COUNT; i++)
            host_propensity_beginnings[i] = propensity_beginning + (i * REACTION_COUNT);
        propensity_beginnings = host_propensity_beginnings;


        // TODO: move calculation from python to here
        thrust::host_vector<propensity_type> host_tmp_propensities(REACTION_COUNT);
        thrust::device_vector<propensity_type> tmp_propensities(REACTION_COUNT);

        {% for i, initial_propensity in enumerate(model.initial_propensities) %}
        host_tmp_propensities[{{ i }}] = {{ initial_propensity }};
        {% endfor %}

        thrust::copy_n(host_tmp_propensities.begin(), REACTION_COUNT, tmp_propensities.begin());

        thrust::copy_n(
            thrust::make_permutation_iterator(
                tmp_propensities.begin(),
                thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_column_index<unsigned int>(REACTION_COUNT))
            ),
            REPLICATION_COUNT * REACTION_COUNT,
            propensities.begin()
        );


        thrust::host_vector<unsigned int> host_tmp_populations(POPULATION_COUNT);
        thrust::device_vector<unsigned int> tmp_populations(POPULATION_COUNT);

        {% for i, initial_population_count in enumerate(model.initial_population_counts) %}
        host_tmp_populations[{{ i }}] = {{ initial_population_count }}u;
        {% endfor %}

        thrust::copy_n(host_tmp_populations.begin(), POPULATION_COUNT, tmp_populations.begin());

        thrust::copy_n(
            thrust::make_permutation_iterator(
                tmp_populations.begin(),
                thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_column_index<unsigned int>(POPULATION_COUNT))
            ),
            REPLICATION_COUNT * POPULATION_COUNT,
            populations.begin()
        );

        // neutral element for multiplication in propensity calculation
        populations[REPLICATION_COUNT * POPULATION_COUNT] = 1u;

        next_reactions = thrust::device_vector<unsigned int>(REPLICATION_COUNT);
        total_propensities = thrust::device_vector<propensity_type>(REPLICATION_COUNT);
        thrust::reduce_by_key(
            thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(REACTION_COUNT)),
            thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(REACTION_COUNT)) + (REPLICATION_COUNT * REACTION_COUNT),
            propensities.begin(),
            thrust::make_discard_iterator(),
            total_propensities.begin()
        );
    }
};


struct can_not_react_anymore {
    __host__ __device__
    bool operator()(thrust::tuple<const propensity_type, const propensity_type> variables) {
        return thrust::get<0>(variables) > MAX_ELAPSED_TIME || thrust::get<1>(variables) <= 0.0;
    }
};


struct update_elapsed_time {
    __host__ __device__
    void operator()(thrust::tuple<propensity_type&, const propensity_type, propensity_type> variables) {
        propensity_type& elapsed_time = thrust::get<0>(variables);
        const propensity_type total_propensity = thrust::get<1>(variables);
        propensity_type random_1 = thrust::get<2>(variables);

        random_1 = random_1 >= 1.0 ? std::nextafterf((propensity_type)1.0, (propensity_type)0.0) : random_1;  // prevent inf elapsed time
        elapsed_time += -LOG((propensity_type)1.0 - random_1) / total_propensity;
    }
};


struct choose_next_reaction {
    __host__ __device__
    void operator()(thrust::tuple<const propensity_type, const propensity_type, const propensity_type*, unsigned int&> variables) {
        const propensity_type total_propensity = thrust::get<0>(variables);
        const propensity_type random_2 = thrust::get<1>(variables);
        const propensity_type* propensities = thrust::get<2>(variables);
        unsigned int& target_reaction = thrust::get<3>(variables);

        propensity_type target_distance = random_2 * total_propensity;
        propensity_type distance = 0.0;
        unsigned int tmp_target_reaction;
        for (tmp_target_reaction = 0; distance < target_distance && tmp_target_reaction < REACTION_COUNT; tmp_target_reaction++)
            distance += propensities[tmp_target_reaction];
        target_reaction = tmp_target_reaction - 1;
    }
};

template <typename T>
struct tuple_add : public thrust::unary_function<thrust::tuple<T, T>, T> {
    __host__ __device__
    T operator()(thrust::tuple<T, T> values) {
        return thrust::get<0>(values) + thrust::get<1>(values);
    }
};


template <typename T>
struct tuple_subtract : public thrust::unary_function<thrust::tuple<T, T>, T> {
    __host__ __device__
    T operator()(thrust::tuple<T, T> values) {
        return thrust::get<0>(values) - thrust::get<1>(values);
    }
};


template <typename T>
struct multiply_by : public thrust::unary_function<T, T> {
    T c;

    __host__ __device__
    multiply_by(T _c) : c(_c) {}

    __host__ __device__
    T operator()(T i) {
        return i * c;
    }
};


struct tuple_add_or_keep_neutral_index : public thrust::unary_function<thrust::tuple<unsigned int, unsigned int>, unsigned int> {
    __host__ __device__
    unsigned int operator()(thrust::tuple<unsigned int, unsigned int> values) {
        unsigned int index = thrust::get<0>(values);
        return index == REPLICATION_COUNT * POPULATION_COUNT ? REPLICATION_COUNT * POPULATION_COUNT : index + thrust::get<1>(values);
    }
};


template <typename T>
struct less_than : public thrust::unary_function<T, bool> {
    T value;

    __host__ __device__
    less_than(T _value) : value(_value) {}

    __host__ __device__
    bool operator()(T i) {
        return i < value;
    }
};


#ifdef FILE_EXPORT
thrust::host_vector<unsigned int> host_indices_map(REPLICATION_COUNT);
thrust::host_vector<propensity_type> host_elapsed_times(REPLICATION_COUNT);
thrust::host_vector<unsigned int> host_populations(REPLICATION_COUNT * POPULATION_COUNT);


void write_to_file(std::ofstream& out_file, Model& model, unsigned int running_simulations_count) {

    thrust::copy_n(
        model.indices_map.begin(),
        running_simulations_count,
        host_indices_map.begin()
    );

    thrust::copy_n(
        model.elapsed_times.begin(),
        REPLICATION_COUNT,
        host_elapsed_times.begin()
    );

    thrust::copy_n(
        model.populations.begin(),
        REPLICATION_COUNT * POPULATION_COUNT,
        host_populations.begin()
    );

    out_file << "\n";

    unsigned int map_index = 0;
    for (unsigned int i = 0; i < REPLICATION_COUNT; i++) {
        if (i > 0)
            out_file << ",";
        if (host_indices_map[map_index] == i) {
            out_file << host_elapsed_times[i];
            for (int population = 0; population < POPULATION_COUNT; population++)
                out_file << "," << host_populations[i * POPULATION_COUNT + population];
            map_index++;
        } else {
            out_file << "{% for i in range(model.population_count) %},{% endfor %}";
        }

    }

}
#endif


int main(int argc, char* argv[]) {

    TIMING_INIT;

    auto init_t1 = std::chrono::high_resolution_clock::now();

    unsigned int running_simulations_count = REPLICATION_COUNT;

    Model model;

    Reactions reactions;

    #ifdef FILE_EXPORT
    thrust::device_vector<unsigned int> file_export_model_indices_map = thrust::device_vector<unsigned int>(REPLICATION_COUNT);
    thrust::sequence(file_export_model_indices_map.begin(), file_export_model_indices_map.end());
    thrust::device_vector<unsigned int> tmp_file_export_model_indices_map = thrust::device_vector<unsigned int>(REPLICATION_COUNT);

    std::string out_filename;
    out_filename += "out_";
    out_filename += std::to_string(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count());
    out_filename += ".csv";
    std::ofstream out_file(out_filename, std::ios_base::out | std::ios_base::trunc);

    for (unsigned int replication = 0; replication < REPLICATION_COUNT; replication++) {
        if (replication)
            out_file << ",";
        out_file << "time_" << replication;
        {% for name in model.reactant_names %}
        out_file << ",{{ name }}_" << replication;
        {% endfor %}
    }

    write_to_file(out_file, model, running_simulations_count);
    #endif

    thrust::device_vector<propensity_type> random_numbers(REPLICATION_COUNT * 2);

    curandGenerator_t gen;
    CURAND_CALL(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
    CURAND_CALL(curandSetPseudoRandomGeneratorSeed(gen, 1234ULL));

    auto init_t2 = std::chrono::high_resolution_clock::now();
    auto init_total_duration = std::chrono::duration_cast<std::chrono::microseconds>(init_t2 - init_t1).count();
    std::cout << ((float)init_total_duration / 1000000.0f) << " seconds (init)\n";

    //unsigned int prev_running_simulations = running_simulations_count;
    std::cout << "running simulations: " << running_simulations_count << "\n";
    auto total_t1 = std::chrono::high_resolution_clock::now();
    unsigned long long total_simulation_steps = 0;
    while (running_simulations_count > 0) {
        total_simulation_steps += running_simulations_count;

        TIMING_START;
        // interval: (0.0, 1.0]
        CURAND_CALL(CURAND_GENERATE_UNIFORM(gen, thrust::raw_pointer_cast(random_numbers.data()), running_simulations_count * 2));
        TIMING_STOP("curand");

        TIMING_START;
        thrust::for_each_n(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(model.elapsed_times.begin(), model.indices_map.begin()),
                    thrust::make_permutation_iterator(model.total_propensities.begin(), model.indices_map.begin()),
                    random_numbers.begin()
                )
            ),
            running_simulations_count,
            update_elapsed_time()
        );
        TIMING_STOP("update_elapsed_time");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count; i++) {
            std::cout << "\n" << model.indices_map[i] << ") propensities: ";
            for (int j = 0; j < REACTION_COUNT; j++)
                std::cout << model.propensities[model.indices_map[i] * REACTION_COUNT + j] << ", ";
        }
        for (int i = 0; i < running_simulations_count; i++)
            std::cout << "\n" << model.indices_map[i] << ") total_propensity: " << model.total_propensities[model.indices_map[i]];
        for (int i = 0; i < running_simulations_count; i++)
            std::cout << "\n" << model.indices_map[i] << ") elapsed_time: " << model.elapsed_times[model.indices_map[i]];
        #endif

        TIMING_START;
        thrust::for_each_n(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(model.total_propensities.begin(), model.indices_map.begin()),
                    random_numbers.begin() + running_simulations_count,
                    thrust::make_permutation_iterator(model.propensity_beginnings.begin(), model.indices_map.begin()),
                    thrust::make_permutation_iterator(model.next_reactions.begin(), model.indices_map.begin())
                )
            ),
            running_simulations_count,
            choose_next_reaction()
        );
        TIMING_STOP("choose_next_reaction");

        #ifdef DEBUG
        for (int i = 0; i < REACTION_COUNT; i++)
            std::cout << "\n> reaction " << i << ": " << thrust::count(thrust::make_permutation_iterator(model.next_reactions.begin(), model.indices_map.begin()), thrust::make_permutation_iterator(model.next_reactions.begin(), model.indices_map.begin()) + running_simulations_count, i);

        for (int i = 0; i < running_simulations_count; i++)
            std::cout << "\n" << model.indices_map[i] << ") next_reaction: " << model.next_reactions[model.indices_map[i]];
        #endif

        TIMING_START;
        // allows aligned access (according to the model.indices_map) to reactions.population_update_indices given the model.next_reactions indices
        auto population_update_index_iterator = thrust::make_transform_iterator(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(
                        thrust::make_transform_iterator(
                            thrust::make_permutation_iterator(model.next_reactions.begin(), model.indices_map.begin()),
                            multiply_by<unsigned int>(MAX_POPULATIONS_TO_UPDATE_PER_REACTION)
                        ),
                        thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_POPULATIONS_TO_UPDATE_PER_REACTION))
                    ),
                    thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_column_index<unsigned int>(MAX_POPULATIONS_TO_UPDATE_PER_REACTION))
                )
            ),
            tuple_add<unsigned int>()
        );
        TIMING_STOP("population_update_index_iterator");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count; i++) {
            std::cout << "\n" << model.indices_map[i] << ") populations: ";
            for (int j = 0; j < POPULATION_COUNT; j++)
                std::cout << model.populations[model.indices_map[i] * POPULATION_COUNT + j] << ", ";
        }

        for (int i = 0; i < running_simulations_count * MAX_POPULATIONS_TO_UPDATE_PER_REACTION; i++) {
            if (i % MAX_POPULATIONS_TO_UPDATE_PER_REACTION == 0)
                std::cout << "\n" << model.indices_map[(i / MAX_POPULATIONS_TO_UPDATE_PER_REACTION)] << ") population_update_index_iterator: ";
            std::cout << population_update_index_iterator[i] << ", ";
        }
        #endif

        TIMING_START;
        auto populations_permutation_iterator = thrust::make_permutation_iterator(
            model.populations.begin(),
            thrust::make_transform_iterator(
                thrust::make_zip_iterator(
                    thrust::make_tuple(
                        thrust::make_permutation_iterator(
                            reactions.population_update_indices.begin(),
                            population_update_index_iterator
                        ),
                        thrust::make_permutation_iterator(
                            thrust::make_transform_iterator(
                                model.indices_map.begin(),
                                multiply_by<unsigned int>(POPULATION_COUNT)
                            ),
                            thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_POPULATIONS_TO_UPDATE_PER_REACTION))
                        )
                    )
                ),
                tuple_add<unsigned int>()  // TODO: maybe keep neutral index instead of adding to prevent overflow? (same for propensity iterators)
            )
        );
        TIMING_STOP("populations_permutation_iterator");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count * MAX_POPULATIONS_TO_UPDATE_PER_REACTION; i++) {
            if (i % MAX_POPULATIONS_TO_UPDATE_PER_REACTION == 0)
                std::cout << "\n" << model.indices_map[(i / MAX_POPULATIONS_TO_UPDATE_PER_REACTION)] << ") populations_permutation_iterator: ";
            std::cout << populations_permutation_iterator[i] << ", ";
        }
        #endif

        TIMING_START;
        thrust::transform_if(
            populations_permutation_iterator,
            populations_permutation_iterator + (running_simulations_count * MAX_POPULATIONS_TO_UPDATE_PER_REACTION),
            thrust::make_permutation_iterator(
                reactions.population_updates.begin(),
                population_update_index_iterator
            ),
            thrust::make_permutation_iterator(
                reactions.population_update_indices.begin(),
                population_update_index_iterator
            ),
            populations_permutation_iterator,
            thrust::plus<int>(),
            less_than<unsigned int>(REPLICATION_COUNT * POPULATION_COUNT)  // filter out invalid population indices that were just added for padding
        );
        TIMING_STOP("update populations");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count; i++) {
            std::cout << "\n" << model.indices_map[i] << ") updated populations: ";
            for (int j = 0; j < POPULATION_COUNT; j++)
                std::cout << model.populations[model.indices_map[i] * POPULATION_COUNT + j] << ", ";
        }
        #endif

        #ifdef FILE_EXPORT
        write_to_file(out_file, model, running_simulations_count);
        #endif

        TIMING_START;
        // allows aligned access (according to the model.indices_map) to reactions.dependency_indices given the model.next_reactions indices
        auto propensity_update_index_iterator = thrust::make_transform_iterator(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(
                        thrust::make_transform_iterator(
                            thrust::make_permutation_iterator(model.next_reactions.begin(), model.indices_map.begin()),
                            multiply_by<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION)
                        ),
                        thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION))
                    ),
                    thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_column_index<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION))
                )
            ),
            tuple_add<unsigned int>()
        );
        TIMING_STOP("propensity_update_index_iterator");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count; i++) {
            std::cout << "\n" << model.indices_map[i] << ") propensities: ";
            for (int j = 0; j < REACTION_COUNT; j++)
                std::cout << model.propensities[model.indices_map[i] * REACTION_COUNT + j] << ", ";
        }

        for (int i = 0; i < running_simulations_count * MAX_DEPENDANT_REACTIONS_PER_REACTION; i++) {
            if (i % MAX_DEPENDANT_REACTIONS_PER_REACTION == 0)
                std::cout << "\n" << model.indices_map[(i / MAX_DEPENDANT_REACTIONS_PER_REACTION)] << ") propensity_update_index_iterator: ";
            std::cout << propensity_update_index_iterator[i] << ", ";
        }
        #endif

        TIMING_START;
        // iterator with aligned access (according to the model.indices_map) to all propensities that should be updated
        // (contains invalid indices for positions that shouldn't be updated)
        auto propensities_permutation_iterator = thrust::make_transform_iterator(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(
                        thrust::make_transform_iterator(
                            model.indices_map.begin(),
                            multiply_by<unsigned int>(REACTION_COUNT)
                        ),
                        thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION))
                    ),
                    thrust::make_permutation_iterator(
                        reactions.dependency_indices.begin(),
                        propensity_update_index_iterator
                    )
                )
            ),
            tuple_add<unsigned int>()
        );
        TIMING_STOP("propensities_permutation_iterator");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count * MAX_DEPENDANT_REACTIONS_PER_REACTION; i++) {
            if (i % MAX_DEPENDANT_REACTIONS_PER_REACTION == 0)
                std::cout << "\n" << model.indices_map[(i / MAX_DEPENDANT_REACTIONS_PER_REACTION)] << ") propensities_permutation_iterator: ";
            std::cout << propensities_permutation_iterator[i] << ", ";
        }
        #endif

        TIMING_START;
        // iterator with aligned access (according to the model.indices_map) to all propensities that should be updated
        // (contains duplicate / redundant propensity indices)
        auto propensities_permutation_iterator_with_redundancies = thrust::make_transform_iterator(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(
                        thrust::make_transform_iterator(
                            model.indices_map.begin(),
                            multiply_by<unsigned int>(REACTION_COUNT)
                        ),
                        thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION))
                    ),
                    thrust::make_permutation_iterator(
                        reactions.dependency_indices_with_redundancies.begin(),
                        propensity_update_index_iterator
                    )
                )
            ),
            tuple_add<unsigned int>()
        );
        TIMING_STOP("propensities_permutation_iterator_with_redundancies");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count * MAX_DEPENDANT_REACTIONS_PER_REACTION; i++) {
            if (i % MAX_DEPENDANT_REACTIONS_PER_REACTION == 0)
                std::cout << "\n" << model.indices_map[(i / MAX_DEPENDANT_REACTIONS_PER_REACTION)] << ") propensities_permutation_iterator_with_redundancies: ";
            std::cout << propensities_permutation_iterator_with_redundancies[i] << ", ";
        }
        #endif

        TIMING_START;
        auto dependant_population_index_iterator = thrust::make_transform_iterator(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(
                        thrust::make_transform_iterator(
                            thrust::make_permutation_iterator(model.next_reactions.begin(), model.indices_map.begin()),
                            multiply_by<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION)
                        ),
                        thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION))
                    ),
                    thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_column_index<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION))
                )
            ),
            tuple_add<unsigned int>()
        );
        TIMING_STOP("dependant_population_index_iterator");

        TIMING_START;
        auto populations_propensities_update_index_iterator = thrust::make_transform_iterator(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(
                        reactions.dependant_population_indices.begin(),
                        dependant_population_index_iterator
                    ),
                    thrust::make_permutation_iterator(
                        thrust::make_transform_iterator(
                            model.indices_map.begin(),
                            multiply_by<unsigned int>(POPULATION_COUNT)
                        ),
                        thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION))
                    )
                )
            ),
            tuple_add_or_keep_neutral_index()
        );
        TIMING_STOP("populations_propensities_update_index_iterator");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count * MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION; i++) {
            if (i % (MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION) == 0)
                std::cout << "\n" << model.indices_map[(i / (MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION))] << ") populations_propensities_update_index_iterator: ";
            std::cout << populations_propensities_update_index_iterator[i] << (i % MAX_DEPENDANT_POPULATIONS_PER_REACTION == 0 ? ", " : " | ");
        }
        #endif

        TIMING_START;
        auto update_propensities_new_ends = thrust::reduce_by_key(
            thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_DEPENDANT_POPULATIONS_PER_REACTION)),
            thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(MAX_DEPENDANT_POPULATIONS_PER_REACTION)) + (running_simulations_count * MAX_DEPENDANT_REACTIONS_PER_REACTION * MAX_DEPENDANT_POPULATIONS_PER_REACTION),
            thrust::make_transform_iterator(
                thrust::make_zip_iterator(
                    thrust::make_tuple(
                        thrust::make_permutation_iterator(
                            model.populations.begin(),
                            populations_propensities_update_index_iterator
                        ),
                        thrust::make_permutation_iterator(
                            reactions.dependant_populations_minus.begin(),
                            dependant_population_index_iterator
                        )
                    )
                ),
                tuple_subtract<int>()
            ),
            thrust::make_discard_iterator(),
            thrust::make_permutation_iterator(
                model.propensities.begin(),
                propensities_permutation_iterator_with_redundancies
            ),  // TODO: remove duplicate calculations from Reactions, because now there are multiple threads writing the same result to the same address
            thrust::equal_to<unsigned int>(),
            thrust::multiplies<propensity_type>()
        );
        TIMING_STOP("update propensities (step 1/2)");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count; i++) {
            std::cout << "\n" << model.indices_map[i] << ") updated propensities (step 1/2): ";
            for (int j = 0; j < REACTION_COUNT; j++)
                std::cout << model.propensities[model.indices_map[i] * REACTION_COUNT + j] << ", ";
        }
        #endif

        assert(update_propensities_new_ends.second == thrust::make_permutation_iterator(model.propensities.begin(), propensities_permutation_iterator_with_redundancies) + (running_simulations_count * MAX_DEPENDANT_REACTIONS_PER_REACTION));

        TIMING_START;
        thrust::transform_if(
            thrust::make_permutation_iterator(
                model.propensities.begin(),
                propensities_permutation_iterator
            ),
            thrust::make_permutation_iterator(
                model.propensities.begin(),
                propensities_permutation_iterator
            ) + (running_simulations_count * MAX_DEPENDANT_REACTIONS_PER_REACTION),
            thrust::make_permutation_iterator(
                reactions.dependency_reaction_rates.begin(),
                propensity_update_index_iterator
            ),
            propensities_permutation_iterator,
            thrust::make_permutation_iterator(
                model.propensities.begin(),
                propensities_permutation_iterator
            ),
            thrust::multiplies<propensity_type>(),
            less_than<unsigned int>(REPLICATION_COUNT * REACTION_COUNT)
        );
        TIMING_STOP("update propensities (step 2/2)");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count; i++) {
            std::cout << "\n" << model.indices_map[i] << ") updated propensities (step 2/2): ";
            for (int j = 0; j < REACTION_COUNT; j++)
                std::cout << model.propensities[model.indices_map[i] * REACTION_COUNT + j] << ", ";
        }
        #endif

        TIMING_START;
        auto running_simulations_propensities_access_iterator_2d = thrust::make_transform_iterator(
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(
                        thrust::make_transform_iterator(
                            model.indices_map.begin(),
                            multiply_by<unsigned int>(REACTION_COUNT)
                        ),
                        thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(REACTION_COUNT))
                    ),
                    thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_column_index<unsigned int>(REACTION_COUNT))
                )
            ),
            tuple_add<unsigned int>()
        );
        TIMING_STOP("running_simulations_propensities_access_iterator_2d");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count * REACTION_COUNT; i++) {
            if (i % REACTION_COUNT == 0)
                std::cout << "\n" << model.indices_map[(i / REACTION_COUNT)] << ") running_simulations_propensities_access_iterator_2d: ";
            std::cout << running_simulations_propensities_access_iterator_2d[i] << ", ";
        }
        #endif

        TIMING_START;
        // TODO: can maybe be improved by only updating with changed propensities
        thrust::reduce_by_key(
            thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(REACTION_COUNT)),
            thrust::make_transform_iterator(thrust::counting_iterator<unsigned int>(0), linear_index_to_row_index<unsigned int>(REACTION_COUNT)) + (running_simulations_count * REACTION_COUNT),
            thrust::make_permutation_iterator(
                model.propensities.begin(),
                running_simulations_propensities_access_iterator_2d
            ),
            thrust::make_discard_iterator(),
            thrust::make_permutation_iterator(model.total_propensities.begin(), model.indices_map.begin())
        );
        TIMING_STOP("calculate total propensities");

        #ifdef DEBUG
        for (int i = 0; i < running_simulations_count; i++)
            std::cout << "\n" << model.indices_map[i] << ") total_propensity: " << model.total_propensities[model.indices_map[i]];
        #endif

        TIMING_START;
        auto tmp_iterator = thrust::remove_if(
            model.indices_map.begin(),
            model.indices_map.begin() + running_simulations_count,
            thrust::make_zip_iterator(
                thrust::make_tuple(
                    thrust::make_permutation_iterator(model.elapsed_times.begin(), model.indices_map.begin()),
                    thrust::make_permutation_iterator(model.total_propensities.begin(), model.indices_map.begin())
                )
            ),
            can_not_react_anymore()
        );
        TIMING_STOP("can_not_react_anymore");

        #ifdef DEBUG
        std::cout << "\n\n";
        #endif

        running_simulations_count = tmp_iterator - model.indices_map.begin();

        // TODO:
        /*if (running_simulations_count < prev_running_simulations) {
            std::cout << "running simulations: " << running_simulations_count << "\n";
            prev_running_simulations = running_simulations_count;
        }*/
    }

    auto total_t2 = std::chrono::high_resolution_clock::now();
    auto total_duration = std::chrono::duration_cast<std::chrono::microseconds>(total_t2 - total_t1).count();
    std::cout << total_simulation_steps << " total steps\n";
    std::cout << ((float)total_duration / 1000000.0f) << " seconds (run)\n";
    std::cout << (((double)total_duration / 1000000.0) / (double)total_simulation_steps * 1000000000.0) << " nanoseconds/step\n";
    std::cout << (((double)total_simulation_steps / ((double)total_duration / 1000000.0)) / 1000000.0) << " steps/microsecond\n";

    #ifdef FILE_EXPORT
    out_file.close();
    #endif

    return EXIT_SUCCESS;
}
