#pragma once

#include <iostream>
#include <fstream>
#include <chrono>
#include <string>
#include <cstring>
#include <cmath>
#include <functional>

#include <assert.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <curand_kernel.h>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>

#include "config.hpp"


typedef char shared_type;


__global__ void simulation_step(propensity_type* elapsed_times_out, int* populations_out, unsigned long long* steps_out) {

    // extern __shared__ shared_type shared[];

    unsigned int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    if (thread_id >= REPLICATION_COUNT)
        return;

    curandState_t state;
    curand_init(1234ULL, thread_id, 0, &state);

    //propensity_type* propensities = (propensity_type*)&shared[REACTION_COUNT * (sizeof(propensity_type) / sizeof(shared_type)) * threadIdx.x];
    //int* populations = (int*)&shared[(REACTION_COUNT * (sizeof(propensity_type) / sizeof(shared_type)) * blockDim.x) + (POPULATION_COUNT * (sizeof(int) / sizeof(shared_type)) * threadIdx.x)];

    propensity_type propensities[REACTION_COUNT];
    int populations[POPULATION_COUNT];

    {% for i, count in enumerate(model.initial_population_counts) %}
    populations[{{ i }}] = {{ count }};
    {% endfor %}

    {% for i, propensity in enumerate(model.initial_propensities) %}
    propensities[{{ i }}] = {{ propensity }};
    {% endfor %}

    propensity_type elapsed_time = 0.0;
    propensity_type total_propensity = {{ sum(model.initial_propensities) }};

    propensity_type random, target_distance, distance;
    unsigned int target_reaction;

    unsigned long long step;
    #ifdef FILE_EXPORT
    for (step = 0; step < MAX_STEP_COUNT; step++) {
    #else
    for (step = 0; elapsed_time <= MAX_ELAPSED_TIME && total_propensity > 0.0; step++) {
    #endif

        #ifdef FILE_EXPORT
        #define STEP step
        #else
        #define STEP (step % 2)
        #endif

        random = curand_uniform_double(&state);
        if (random >= 1.0)
            random = std::nextafter((propensity_type)1.0, (propensity_type)0.0);  // prevent inf elapsed time
        elapsed_time += -LOG((propensity_type)1.0 - random) / total_propensity;

        elapsed_times_out[(size_t)STEP * REPLICATION_COUNT + thread_id] = elapsed_time;

        target_distance = curand_uniform_double(&state) * total_propensity;
        distance = 0.0;
        for (target_reaction = 0; distance < target_distance && target_reaction < REACTION_COUNT; target_reaction++)
            distance += propensities[target_reaction];
        target_reaction--;

        switch (target_reaction) {
        {% for reaction_index, reaction in enumerate(model.reactions) %}
        case {{ loop.index0 }}: {
            {% for target_index in sorted(reaction.keys()) %}
            {% if reaction[target_index] < 0 %}
            populations[{{ target_index }}] -= {{ -reaction[target_index] }};
            {% else %}
            populations[{{ target_index }}] += {{ reaction[target_index] }};
            {%endif%}
            populations_out[(size_t)STEP * REPLICATION_COUNT * POPULATION_COUNT + thread_id * POPULATION_COUNT + {{ target_index }}] = populations[{{ target_index }}];
            {% endfor %}

            {% for dependency_index in model.dependency_graph[reaction_index] %}
            //total_propensity -= propensities[{{ dependency_index }}];
            {% endfor %}

            {% for dependency_index in model.dependency_graph[reaction_index] %}
            //total_propensity -= propensities[{{ dependency_index }}];
            propensities[{{ dependency_index }}] = (propensity_type){{ model.reaction_rates[dependency_index] }}{% for dependency, minus in zip(model.dependant_populations[dependency_index], model.dependant_populations_minus[dependency_index]) %} * (populations[{{ dependency }}] - {{ minus }}){% endfor %};
            //total_propensity += propensities[{{ dependency_index }}];
            {% endfor %}

            {% for dependency_index in model.dependency_graph[reaction_index] %}
            //total_propensity += propensities[{{ dependency_index }}];
            {% endfor %}

            break;
        }
        {% endfor %}
        default:
            assert(0 && "invalid target_reaction");
            break;
        }

        total_propensity = 0.0;
        for (int i = 0; i < REACTION_COUNT; i++)
            total_propensity += propensities[i];

    }

    steps_out[thread_id] = step;

}


/*struct block_size_to_dynamic_shared_memory_size : public std::unary_function<int, size_t> {
    size_t operator()(int block_size) {
        return (((size_t)POPULATION_COUNT * sizeof(int)) + ((size_t)REACTION_COUNT * sizeof(propensity_type))) * (size_t)block_size;
    }
};*/


int main(int argc, char* argv[]) {

    TIMING_INIT;

    auto init_t1 = std::chrono::high_resolution_clock::now();

    const size_t memory_size_per_thread = (
        ((size_t)POPULATION_COUNT * sizeof(int))  // int populations[POPULATION_COUNT];
        + ((size_t)REACTION_COUNT * sizeof(propensity_type))  // propensity_type propensities[REACTION_COUNT];
        + (sizeof(unsigned int) * 2)  // unsigned int thread_id, target_reaction;
        + sizeof(curandState_t)  // curandState_t state;
        + (sizeof(propensity_type) * 5)  // propensity_type elapsed_time, total_propensity, random, target_distance, distance;
        + sizeof(unsigned long long)  // unsigned long long step;
    );

    int max_shared_memory;
    CUDA_CALL(cudaDeviceGetAttribute(&max_shared_memory, cudaDevAttrMaxSharedMemoryPerBlock, 0));
    int block_size_limit = max_shared_memory / memory_size_per_thread;

    int block_size;
    int min_grid_size;

    // CUDA_CALL(cudaOccupancyMaxPotentialBlockSizeVariableSMem(&min_grid_size, &block_size, simulation_step, block_size_to_dynamic_shared_memory_size(), block_size_limit));
    CUDA_CALL(cudaOccupancyMaxPotentialBlockSize(&min_grid_size, &block_size, simulation_step, 0, block_size_limit));

    int grid_size = (REPLICATION_COUNT + block_size - 1) / block_size;

    #ifdef DEBUG
    std::cout << "max_shared_memory: " << max_shared_memory << "\n";
    std::cout << "block_size_limit: " << block_size_limit << "\n";
    std::cout << "min_grid_size: " << min_grid_size << "\n";
    std::cout << "block_size: " << block_size << "\n";
    std::cout << "grid_size: " << grid_size << "\n";
    std::cout << "per block memory usage: " << (memory_size_per_thread * block_size);
    std::cout << " (" << ((double)memory_size_per_thread * block_size / max_shared_memory * 100.0) << "%)" << "\n";
    #endif

    thrust::device_vector<propensity_type> elapsed_times(REPLICATION_COUNT * MAX_STEP_COUNT, -1.0);
    thrust::device_vector<int> populations(REPLICATION_COUNT * MAX_STEP_COUNT * POPULATION_COUNT, -1);
    thrust::device_vector<unsigned long long> steps(REPLICATION_COUNT, 0);

    auto init_t2 = std::chrono::high_resolution_clock::now();
    auto init_total_duration = std::chrono::duration_cast<std::chrono::microseconds>(init_t2 - init_t1).count();
    std::cout << ((float)init_total_duration / 1000000.0f) << " seconds (init)\n";

    std::cout << "running simulations: " << REPLICATION_COUNT << "\n";
    auto t1 = std::chrono::high_resolution_clock::now();

    simulation_step<<<grid_size, block_size/*, block_size_to_dynamic_shared_memory_size()(block_size)*/>>>(
        thrust::raw_pointer_cast(elapsed_times.data()),
        thrust::raw_pointer_cast(populations.data()),
        thrust::raw_pointer_cast(steps.data())
    );

    // note: the GPU driver may kill the kernel if the GPU is connected to a display and the execution takes too long

    cudaDeviceSynchronize();

    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) {
        printf("Error: %s\n", cudaGetErrorString(err));
        return -1;
    }

    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

    unsigned long long total_simulation_steps = thrust::reduce(steps.begin(), steps.end());

    std::cout << total_simulation_steps << " total steps\n";
    std::cout << ((float)duration / 1000000.0f) << " seconds (run)\n";
    std::cout << (((double)duration / 1000000.0) / (double)total_simulation_steps * 1000000000.0) << " nanoseconds/step\n";
    std::cout << (((double)total_simulation_steps / ((double)duration / 1000000.0)) / 1000000.0) << " steps/microsecond\n";

    #ifdef FILE_EXPORT
    thrust::host_vector<propensity_type> host_elapsed_times(elapsed_times);
    thrust::host_vector<int> host_populations(populations);
    thrust::host_vector<int> host_prev_populations(host_populations.begin(), host_populations.begin() + (REPLICATION_COUNT * POPULATION_COUNT));
    std::string out_filename;
    out_filename += "out_";
    out_filename += std::to_string(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count());
    out_filename += ".csv";
    std::ofstream out_file(out_filename, std::ios_base::out | std::ios_base::trunc);
    // out_file << std::fixed;
    for (int replication = 0; replication < REPLICATION_COUNT; replication++) {
        if (replication)
            out_file << ",";
        out_file << "time_" << replication;
        {% for name in model.reactant_names %}
        out_file << ",{{ name }}_" << replication;
        {% endfor %}
    }
    for (int step = 0; step < MAX_STEP_COUNT; step++) {
        std::cout << "writing to file - step " << (step + 1) << "/" << MAX_STEP_COUNT << "\n";
        out_file << "\n";
        for (int replication = 0; replication < REPLICATION_COUNT; replication++) {
            propensity_type elapsed_time = host_elapsed_times[REPLICATION_COUNT * step + replication];
            if (replication > 0)
                out_file << ",";
            if (elapsed_time < 0.0) {
                out_file << "{% for i in range(model.population_count) %},{% endfor %}";
            } else {
                out_file << elapsed_time;
                for (int p = 0; p < POPULATION_COUNT; p++) {
                    int population = host_populations[step * REPLICATION_COUNT * POPULATION_COUNT + replication * POPULATION_COUNT + p];
                    int& prev_population = host_prev_populations[replication * POPULATION_COUNT + p];
                    if (population < 0)
                        population = prev_population;
                    prev_population = population;
                    out_file << "," << population;
                }
            }
        }
    }
    out_file.close();
    #endif

    return EXIT_SUCCESS;
}
