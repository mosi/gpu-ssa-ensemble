#pragma once

{% if not single_precision %}// {% endif %}#define SINGLE_PRECISION

#ifdef SINGLE_PRECISION
typedef float propensity_type;
#define CURAND_GENERATE_UNIFORM curandGenerateUniform
#define LOG logf
#else
typedef double propensity_type;
#define CURAND_GENERATE_UNIFORM curandGenerateUniformDouble
#define LOG log
#endif


#define CUDA_CALL(x) do { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)
#define CURAND_CALL(x) do { if((x)!=CURAND_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)


#define REPLICATION_COUNT {{ model.replication_count }}u
#define MAX_STEP_COUNT 30500ull
#define REACTION_COUNT {{ model.reaction_count }}u
#define POPULATION_COUNT {{ model.population_count }}u
#define MAX_ELAPSED_TIME {{ model.max_elapsed_time }}
#define MAX_POPULATIONS_TO_UPDATE_PER_REACTION {{ model.max_populations_to_update_per_reaction }}u
#define MAX_DEPENDANT_REACTIONS_PER_REACTION {{ model.max_dependant_reactions_per_reaction }}u
#define MAX_DEPENDANT_POPULATIONS_PER_REACTION {{ model.max_dependant_populations_per_reaction }}u

#define FILE_EXPORT_QUEUE_SIZE 10u

{% if not file_export %}// {% endif %}#define FILE_EXPORT
{% if not debug %}// {% endif %}#define DEBUG
{% if not timing %}// {% endif %}#define TIMING
{% if not disable_runtime_sorting %}// {% endif %}#define DISABLE_RUNTIME_SORTING

#ifndef FILE_EXPORT
#undef MAX_STEP_COUNT
#define MAX_STEP_COUNT 2ull
#endif

#ifdef TIMING
#define TIMING_INIT std::chrono::time_point<std::chrono::high_resolution_clock> _t1, _t2
#define TIMING_START _t1 = std::chrono::high_resolution_clock::now()
#define TIMING_STOP(NAME) do { _t2 = std::chrono::high_resolution_clock::now(); \
    { auto _duration = std::chrono::duration_cast<std::chrono::microseconds>(_t2 - _t1).count(); \
    std::cout << ((float)_duration / 1000000.0f) << " seconds (" << NAME << ")\n"; } } while(0)
#else
#define TIMING_INIT
#define TIMING_START
#define TIMING_STOP(NAME)
#endif

#define CUDA_CALL(x) do { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)
#define CURAND_CALL(x) do { if((x)!=CURAND_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)
