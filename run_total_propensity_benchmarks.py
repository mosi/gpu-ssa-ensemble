from run_benchmarks import *


def main():
    runs = 1
    count_reactions_replication_count = 2 ** 15
    fat_kernel_template = "coarse_grained_parallelization.cu"
    fat_kernel_incremental_total_propensity_template = "coarse_grained_parallelization_with_incremental_total_propensity_updates.cu"

    count_reactions(model_file="multisite2.reaction_model",
                    max_time=4.0,
                    replication_count=count_reactions_replication_count)

    run_benchmark(model_file="multisite2.reaction_model",
                  max_time=4.0,
                  replication_exponents=[16],
                  templates=[("Fat Kernel", fat_kernel_template),
                             ("Fat Kernel Test", fat_kernel_incremental_total_propensity_template),
                             ],
                  runs=runs)

    count_reactions(model_file="multistate.reaction_model",
                    max_time=10.0,
                    replication_count=count_reactions_replication_count)

    run_benchmark(model_file="multistate.reaction_model",
                  max_time=10.0,
                  replication_exponents=[20],
                  templates=[("Fat Kernel", fat_kernel_template),
                             ("Fat Kernel Test", fat_kernel_incremental_total_propensity_template),
                             ],
                  runs=runs)

    count_reactions(model_file="decay_dimerization.reaction_model",
                    max_time=10.0,
                    replication_count=count_reactions_replication_count)

    run_benchmark(model_file="decay_dimerization.reaction_model",
                  max_time=10.0,
                  replication_exponents=[20],
                  templates=[("Fat Kernel", fat_kernel_template),
                             ("Fat Kernel Test", fat_kernel_incremental_total_propensity_template),
                             ],
                  runs=runs)

    run_sir_regions_benchmark(replication_count=(2 ** 15),
                              max_time=20.0,
                              regions=[1, 500],
                              templates=[("Fat Kernel", fat_kernel_template),
                                         ("Fat Kernel Test", fat_kernel_incremental_total_propensity_template),
                                         ],
                              runs=runs,
                              count_reactions_replication_count=count_reactions_replication_count)

    run_sir_regions_benchmark(replication_count=(2 ** 20),
                              max_time=10.0,
                              regions=[1, 50],
                              templates=[("Fat Kernel", fat_kernel_template),
                                         ("Fat Kernel Test", fat_kernel_incremental_total_propensity_template),
                                         ],
                              runs=runs,
                              count_reactions_replication_count=count_reactions_replication_count)


if __name__ == "__main__":
    main()
