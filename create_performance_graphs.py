import json
import argparse
import os
import pathlib
from math import ceil

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats


# source: https://stackoverflow.com/questions/15033511/compute-a-confidence-interval-from-sample-data
def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return h


def translate_method_name(name):
    if name == "Fat Kernel":
        return "without sorting"
    if name == "Fat Kernel + Sortierung":
        return "logical sorting"
    if name == "Fat Kernel + Sortierung - Overhead":
        return "logical sorting minus overhead"
    if name == "Fat Kernel + Zustands-Sortierung":
        return "physical sorting"
    if name == "Fat Kernel + Zustands-Sortierung - Overhead":
        return "physical sorting minus overhead"
    if name == "CPU":
        return "CPU"
    if name == "gpu_direct_method":
        return "GPU direct method"
    if name == "Fat Kernel + Main Loop":
        return "without sorting (2)"
    raise ValueError(name)


def get_method_color(name):
    if name == "Fat Kernel":
        return "C1"
    if name == "Fat Kernel + Sortierung":
        return "C0"
    if name == "Fat Kernel + Sortierung - Overhead":
        return "#8fbbd9"
    if name == "Fat Kernel + Zustands-Sortierung":
        return "C2"
    if name == "Fat Kernel + Zustands-Sortierung - Overhead":
        return "#95cf95"
    if name == "CPU":
        return "C3"
    if name == "gpu_direct_method":
        return "C4"
    if name == "Fat Kernel + Main Loop":
        return "C5"
    raise ValueError(name)


def get_method_order(name):
    if name == "Fat Kernel":
        return 1
    if name == "Fat Kernel + Sortierung":
        return 3
    if name == "Fat Kernel + Sortierung - Overhead":
        return 4
    if name == "Fat Kernel + Zustands-Sortierung":
        return 5
    if name == "Fat Kernel + Zustands-Sortierung - Overhead":
        return 6
    if name == "CPU":
        return 0
    if name == "gpu_direct_method":
        return 7
    if name == "Fat Kernel + Main Loop":
        return 2
    raise ValueError(name)


def get_label_order(idx):
    if idx == 0:
        return 0
    if idx == 1:
        return 6
    if idx == 3:
        return 2
    if idx == 4:
        return 3
    if idx == 5:
        return 4
    if idx == 6:
        return 5
    if idx == 7:
        return 1
    if idx == 2:
        return 7
    raise ValueError(idx)


def get_performance(steps, time, is_time_per_step=False, million=True):
    if is_time_per_step:
        if million:
            return time / steps * 1000000.0
        else:
            return time / steps
    else:
        if million:
            return steps / time / 1000000.0
        else:
            return steps / time


def create_bar_charts(json_file, cpu_steps=None, cpu_time=None, cpu_file=None, is_time_per_step=False, separate_legend=False):
    json_file = pathlib.Path(json_file).resolve()
    with open(json_file, "r") as f:
        data = json.load(f)
    print(json_file.name)

    #colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    #cpu_color = colors[len(data["data"]) % len(colors)]

    plt.rcParams.update({'font.size': 16})
    fig, ax = plt.subplots()
    ax.set_xlabel(data["meta"]["x-label"])
    if is_time_per_step:
        ax.set_ylabel("μs per step")
    else:
        ax.set_ylabel("Million steps per second")
    X_labels = list(data["data"][list(data["data"].keys())[0]].keys())
    try:
        int(X_labels[0])
    except Exception:
        X_labels.sort()
    else:
        X_labels.sort(key=lambda x: int(x))
    X = np.arange(len(X_labels))
    print(X_labels)

    cpu_performance = None
    if cpu_steps is not None and cpu_time is not None:
        cpu_performance = [get_performance(cpu_steps, cpu_time, is_time_per_step)]
    else:
        if cpu_file is not None:
            with open(cpu_file, "r") as f:
                cpu_data = json.load(f)
            if data["meta"]["x-label"] == "Regions":
                data["data"]["CPU"] = {
                    str(region_count): {
                        "steps": cpu_data["SIR_{}.reaction_model;{}".format(region_count, int(data["meta"]["max_time"]))]["num_steps"],
                        "run_time": cpu_data["SIR_{}.reaction_model;{}".format(region_count, int(data["meta"]["max_time"]))]["t_reported"]
                    } for region_count in X_labels
                }
            else:
                d = cpu_data.get("{};{}".format(data["meta"]["model_file"], int(data["meta"]["max_time"])))
                if d is not None:
                    cpu_performance = [get_performance(steps, run_time, is_time_per_step) for steps, run_time in zip(d["num_steps"], d["t_reported"])]
    print("CPU:", cpu_performance)

    width = 0.8 / len(data["data"]) # bar width

    performances = {}
    sorting_method_performance_minus_sorting_time = None
    for i, (template_name, values) in enumerate(sorted(data["data"].items(), key=lambda x: get_method_order(x[0]))):
        performances[template_name] = [[get_performance(steps, run_time, is_time_per_step)
                                        for steps, run_time
                                        in zip(values.get(k, {"steps": []})["steps"], values.get(k, {"run_time": []})["run_time"])]  # TODO: default values cause RuntimeWarning
                                       for k in X_labels]
        means = [np.mean(l) for l in performances[template_name]]
        print(f"{translate_method_name(template_name)}: {means}")
        ax.bar(X - ((len(data["data"]) - 1)*width/2) + (i*width), means, width,
               label=translate_method_name(template_name),
               yerr=[mean_confidence_interval(l) for l in performances[template_name]], capsize=2,
               color=get_method_color(template_name))
        #performances[template_name] = [np.mean(l) for l in performances[template_name]]

        if template_name == "Fat Kernel + Sortierung" or template_name == "Fat Kernel + Zustands-Sortierung":
            template_name += " - Overhead"
            sorting_method_performance_minus_sorting_time = [[get_performance(steps, run_time, is_time_per_step)
                                                              for steps, run_time
                                                              in zip(values.get(k, {"steps": []})["steps"],
                                                                     [run_time - sort_time for run_time, sort_time in
                                                                      zip(values.get(k, {"run_time": []})["run_time"],
                                                                          values.get(k, {"sort_time": []})["sort_time"])])]
                                                             for k in X_labels]
            means_without_sorting = [np.mean(l) for l in sorting_method_performance_minus_sorting_time]
            means_with_sorting = means
            diff = [a - b for a, b in zip(means_without_sorting, means_with_sorting)]
            print(f"(without sorting included) {translate_method_name('Fat Kernel + Sortierung')}: {means_without_sorting}")
            if is_time_per_step:
                bottom = 0.0
                height = means_without_sorting
            else:
                bottom = means_with_sorting
                height = diff
            ax.bar(X - ((len(data["data"]) - 1)*width/2) + (i*width), height, width,
                   label=translate_method_name(template_name), bottom=bottom, capsize=2,
                   color=get_method_color(template_name))
    print()

    if cpu_performance is not None:
        ax.axhline(y=np.mean(cpu_performance), linestyle="--", label="CPU", c=get_method_color("CPU"))

    ax.set_xticks(X)
    ax.set_xticklabels(X_labels)
    if separate_legend:
        legend_fig, legend_ax = plt.subplots(figsize=(1e-8, 1e-8))
        handles, labels = ax.get_legend_handles_labels()
        handles, labels = zip(*((handle, label)
                                for _, (handle, label)
                                in sorted(enumerate(zip(handles, labels)), key=lambda k: get_label_order(k[0]))))
        legend_ax.axis("off")
        legend_ax.legend(handles, labels, ncol=ceil(len(labels) / 2))
        legend_fig.tight_layout()
        legend_fig.savefig("{}-legend.pdf".format(os.path.splitext(str(json_file))[0]), dpi=300, bbox_inches="tight")
    else:
        ax.legend()
    fig.tight_layout()
    fig.savefig("{}-absolute.pdf".format(os.path.splitext(str(json_file))[0]), dpi=300, bbox_inches="tight")

    if is_time_per_step:
        return  # relative inverse plots not supported (yet)

    if cpu_performance is not None:
        performances["CPU"] = [cpu_performance for k in X_labels]
    yerr = {}
    for i in range(len(X_labels)):
        max_value = max(np.mean(performances[template_name][i]) for template_name in performances.keys())
        for template_name in performances.keys():
            err = yerr.get(template_name, [])
            err.append(mean_confidence_interval(performances[template_name][i]) / max_value * 100.0)
            yerr[template_name] = err
            performances[template_name][i] = np.mean(performances[template_name][i]) / max_value * 100.0
    if cpu_performance is not None:
        cpu_performances_relative = [np.mean(x) for x in performances["CPU"]]
        del performances["CPU"]

    fig2, ax2 = plt.subplots()
    ax2.set_xlabel(data["meta"]["x-label"])
    ax2.set_ylabel("Steps/second\nnormalized to fastest in %")

    for i, (template_name, values) in enumerate(sorted(performances.items(), key=lambda x: get_method_order(x[0]))):
        ax2.bar(X - ((len(data["data"]) - 1)*width/2) + (i*width), values, width, label=translate_method_name(template_name),
                yerr=yerr[template_name], capsize=2, color=get_method_color(template_name))

    if cpu_performance is not None:
        for i, x in enumerate(X):
            if i == 0:
                ax2.plot([x - 0.5, x + 0.5], [cpu_performances_relative[i]] * 2, linestyle="--", c=get_method_color("CPU"), label="CPU")
            else:
                ax2.plot([x - 0.5, x + 0.5], [cpu_performances_relative[i]] * 2, linestyle="--", c=get_method_color("CPU"))

    ax2.set_xticks(X)
    ax2.set_xticklabels(X_labels)
    if not separate_legend:
        if "multisite2" in str(json_file):
            ax2.legend(loc="lower center")
        else:
            ax2.legend()
    fig2.tight_layout()
    fig2.savefig("{}-relative.pdf".format(os.path.splitext(str(json_file))[0]), dpi=300, bbox_inches="tight")


def create_performance_graph(json_file, cpu_file=None, is_time_per_step=False, separate_legend=False, million=False):
    if separate_legend:
        raise NotImplementedError

    json_file = pathlib.Path(json_file).resolve()
    with open(json_file, "r") as f:
        data = json.load(f)

    cpu_performance = None
    if cpu_file is not None:
        with open(cpu_file, "r") as f:
            cpu_data = json.load(f)
        d = cpu_data.get("{};{}".format(data["meta"]["model_file"], int(data["meta"]["max_time"])))
        if d is not None:
            cpu_performance = [get_performance(steps, run_time, is_time_per_step, million) for steps, run_time in zip(d["num_steps"], d["t_reported"])]

    plt.rcParams.update({'font.size': 16})
    fig, ax = plt.subplots(figsize=(12, 5))
    ax.set_xscale("log", base=10)
    ax2 = ax.twiny()
    ax2.set_xscale("log", base=2)
    ax.set_xlabel(data["meta"]["x-label"])
    if is_time_per_step:
        if million:
            ax.set_ylabel("μs per step")
        else:
            ax.set_ylabel("seconds per step")
    else:
        if million:
            ax.set_ylabel("Million steps per second")
        else:
            ax.set_ylabel("Steps per second")
    ax.set_yscale("log", base=10)
    ax2.set_yscale("log", base=10)
    X_labels, index, _ = max(((list(data["data"][label].keys()), label, len(data["data"][label])) for label in data["data"]), key=lambda x: x[2])
    print(X_labels)
    #X = np.arange(len(X_labels))
    X = [2 ** n for n in range(28)]

    if cpu_performance is not None:
        ax.axhline(y=np.mean(cpu_performance), linestyle="--", label=translate_method_name("CPU"), c=get_method_color("CPU"))

    for label, values in sorted(data["data"].items(), key=lambda x: get_method_order(x[0])):
        performances = [np.mean([get_performance(steps, run_time, is_time_per_step, million)
                                 for steps, run_time in zip(values[k]["steps"], values[k]["run_time"])])
                        for k in X_labels[:len(values)]]
        print(f"{translate_method_name(label)}: {performances}")
        ax2.plot(X[:len(values)], performances, color="w", alpha=0)
        ax.loglog(X[:len(values)], performances, label=translate_method_name(label), marker="o", color=get_method_color(label))

        if label == "Fat Kernel + Sortierung" or label == "Fat Kernel + Zustands-Sortierung":
            label += " - Overhead"
            performances = [np.mean([get_performance(steps, run_time, is_time_per_step, million)
                             for steps, run_time
                             in zip(values.get(k, {"steps": []})["steps"],
                                    [run_time - sort_time for run_time, sort_time in
                                     zip(values.get(k, {"run_time": []})["run_time"],
                                         values.get(k, {"sort_time": []})["sort_time"])])])
                            for k in X_labels[:len(values)]]
            print(f"{translate_method_name(label)}: {performances}")
            ax2.plot(X[:len(values)], performances, color="w", alpha=0)
            ax.loglog(X[:len(values)], performances, label=translate_method_name(label), marker="o", color=get_method_color(label))
    #ax.plot([1, 2 ** 26], [1, 2 ** 26])

    #ax.set_xticks(X)
    #ax.tick_params(axis="x", labelrotation=90)
    #ax.set_xticklabels(X_labels)
    handles, labels = ax.get_legend_handles_labels()
    handles, labels = zip(*((handle, label)
                            for _, (handle, label)
                            in sorted(enumerate(zip(handles, labels)), key=lambda k: get_label_order(k[0]))))
    ax.legend(handles, labels)
    #ax.grid(True, which="both", ls="-", linewidth=0.5)
    locmin = mpl.ticker.LogLocator(base=10.0, subs=(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9), numticks=12)
    ax.xaxis.set_minor_locator(locmin)
    ax.xaxis.set_minor_formatter(mpl.ticker.NullFormatter())
    fig.tight_layout()
    fig.savefig("{}.pdf".format(os.path.splitext(str(json_file))[0]), dpi=600, bbox_inches="tight")


def get_argument_parser():
    parser = argparse.ArgumentParser(description="Plot contents of a result json file",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("json_filepaths", metavar="JSON_FILES", nargs="+", help="One or multiple json result files")
    parser.add_argument("--line-graph", action="store_true", help="Create a line graph instead of a bar chart")
    parser.add_argument("--cpu-results", help="Path to cpu results json file")
    parser.add_argument("--time-per-step", action="store_true", help="Display performance as time per step instead of steps per time")
    parser.add_argument("--separate-legend", action="store_true", help="Save the legend as a separate file")
    #parser.add_argument("--include-code-gen-time", action="store_true", help="TODO")  # TODO
    #parser.add_argument("--include-compile-time", action="store_true", help="TODO")  # TODO
    #parser.add_argument("--include-init-time", action="store_true", help="TODO")  # TODO
    return parser


def main():
    ACM_COLORS = [
        "#FFD600",
        "#FC9200",
        "#FD1B14",
        "#83CEE2",
        "#74BC09",
        "#65016B",
        "#09357A"
    ]
    # mpl.rcParams["axes.prop_cycle"] = mpl.cycler(color=ACM_COLORS)

    mpl.rcParams["mathtext.fontset"] = "custom"
    mpl.rcParams["mathtext.rm"] = "Linux Libertine G"
    mpl.rcParams["mathtext.it"] = "Linux Libertine G:italic"
    mpl.rcParams["mathtext.bf"] = "Linux Libertine G:bold"

    plt.rcParams["font.family"] = "Linux Libertine G"

    mpl.rcParams["mathtext.fontset"] = "custom"

    mpl.rcParams["figure.figsize"] = (6.4, 4.0)

    parser = get_argument_parser()
    args = parser.parse_args()

    plotting_func = create_bar_charts
    if args.line_graph:
        plotting_func = create_performance_graph

    for filepath in args.json_filepaths:
        plotting_func(json_file=filepath,
                      cpu_file=args.cpu_results,
                      is_time_per_step=args.time_per_step,
                      separate_legend=args.separate_legend)


if __name__ == "__main__":
    main()
