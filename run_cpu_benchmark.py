import subprocess
import re
import json
import os
from pathlib import Path
import time


models = [("SIR_{}.reaction_model".format(i), 10.0) for i in [1, 2, 5, 10, 20, 50]]
models += [("SIR_{}.reaction_model".format(i), 20.0) for i in [1, 2, 5, 10, 20, 50, 100, 200, 500]]
models += [("decay_dimerization.reaction_model", 10.0),
           ("multistate.reaction_model", 10.0),
           ("multisite2.reaction_model", 4.0)]

runs = 10


def append_value(r, group, value):
    l = r.get(group, [])
    l.append(value)
    r[group] = l


def main():

    p = re.compile(
        (r"t_parsing *\| *(?P<t_parsing>([0-9\.]*)).*"
         r"t_sort *\| *(?P<t_sort>([0-9\.]*)).*"
         r"t_compile *\| *(?P<t_compile>([0-9\.]*)).*"
         r"t_total_run *\| *(?P<t_total_run>([0-9\.]*)).*"
         r"t_reported *\| *(?P<t_reported>([0-9\.]*)).*"
         r"num_steps *\| *(?P<num_steps>([0-9\.]*))"),
        re.S
    )

    results = {}

    base_dir = Path(__file__).resolve().parent
    results_file = base_dir / "results" / f"CPU - {time.time()}.json"

    os.chdir(base_dir / "reproduce_code_generation")

    for model_file, max_time in models:
        command = [
            "python3",
            "runSimulation.py",
            "--sort",
            "--steps=2",
            "--lto",
            "--target=gcc",
            "--singlefile",
            f"--until={max_time}",
            f"../models/{model_file}"
        ]

        key = f"{model_file};{int(max_time)}"

        for i in range(runs):
            print("[sorting]", i, "-", model_file, "-", max_time)
            try:
                os.remove("reaction_counts.toml")
            except FileNotFoundError:
                pass
            # single run for sorting:
            subprocess.check_output(command)

            print(i, "-", model_file, "-", max_time)
            output = subprocess.check_output(command).decode('utf-8')
            match = p.search(output)

            r = results.get(key, {})
            for group in ["t_parsing", "t_sort", "t_compile", "t_total_run", "t_reported", "num_steps"]:
                append_value(r, group, float(match.group(group)) if group != "num_steps" else int(match.group(group)))
            results[key] = r

            with open(results_file, "w") as f:
                json.dump(results, f, indent=4)


if __name__ == "__main__":
    main()
